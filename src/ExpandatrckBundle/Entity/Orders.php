<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\OrdersRepository")
 */
class Orders {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**

     * @ORM\OneToMany(targetEntity="Service", mappedBy="Orders")
     * 
     */
    private $Service;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Orders")
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="ServiceOrder", mappedBy="Orders")
     * 
     */
    private $ServiceOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="quote_date", type="datetime")
     */
    private $quoteDate;

    /**
     * @var string
     *
     * @ORM\Column(name="expires", type="datetime")
     */
    private $expires;

    /**
     * @var string
     *
     * @ORM\Column(name="quotation", type="string", length=255,nullable=true)
     */
    private $quotation;

    /**
     * @var string
     *
     * @ORM\Column(name="quoteno", type="string", length=255)
     */
    private $quoteno;

    /**
     * @var string
     *
     * @ORM\Column(name="service_cost ", type="string")
     */
    private $service_cost;

    /**
     * @var string
     *
     * @ORM\Column(name="travel_cost", type="string")
     */
    private $travel_cost;

    /**
     * @var string
     *
     * @ORM\Column(name="other ", type="string")
     */
    private $other;

    /**
     * @var string
     *
     * @ORM\Column(name="other_text ", type="text",nullable=true)
     */
    private $other_text;

    /**
     * @var string
     *
     * @ORM\Column(name="description ", type="text",nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_include", type="integer")
     */
    private $is_include = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="manual_cost", type="string",nullable=true)
     */
    private $manual_cost;

    /**
     * @var integer
     *
     * @ORM\Column(name="subscription", type="integer")
     */
    private $subscription = 0;
   
    /**
     * @var integer
     *
     * @ORM\Column(name="taxstatus", type="integer")
     */
    private $taxstatus = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_manual_include", type="integer",options={"default" : 1})
     */
    private $is_manual_include = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="special_price", type="string",nullable=true)
     */
    private $special_price;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Orders
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Orders
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Orders
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Orders
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set quoteDate
     *
     * @param \DateTime $quoteDate
     *
     * @return Orders
     */
    public function setQuoteDate($quoteDate) {
        $this->quoteDate = $quoteDate;

        return $this;
    }

    /**
     * Get quoteDate
     *
     * @return \DateTime
     */
    public function getQuoteDate() {
        return $this->quoteDate;
    }

    /**
     * Set expires
     *
     * @param string $expires
     *
     * @return Orders
     */
    public function setExpires($expires) {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires
     *
     * @return string
     */
    public function getExpires() {
        return $this->expires;
    }

    /**
     * Set quotation
     *
     * @param string $quotation
     *
     * @return Orders
     */
    public function setQuotation($quotation) {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation
     *
     * @return string
     */
    public function getQuotation() {
        return $this->quotation;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->Service = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add service
     *
     * @param \ExpandatrckBundle\Entity\Service $service
     *
     * @return Orders
     */
    public function addService(\ExpandatrckBundle\Entity\Service $service) {
        $this->Service[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \ExpandatrckBundle\Entity\Service $service
     */
    public function removeService(\ExpandatrckBundle\Entity\Service $service) {
        $this->Service->removeElement($service);
    }

    /**
     * Get service
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getService() {
        return $this->Service;
    }

    /**
     * Set user
     *
     * @param \ExpandatrckBundle\Entity\User $user
     *
     * @return Orders
     */
    public function setUser(\ExpandatrckBundle\Entity\User $user = null) {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ExpandatrckBundle\Entity\User
     */
    public function getUser() {
        return $this->User;
    }

    /**
     * Add serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     *
     * @return Orders
     */
    public function addServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder) {
        $this->ServiceOrder[] = $serviceOrder;

        return $this;
    }

    /**
     * Remove serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     */
    public function removeServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder) {
        $this->ServiceOrder->removeElement($serviceOrder);
    }

    /**
     * Get serviceOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceOrder() {
        return $this->ServiceOrder;
    }

    /**
     * Set quoteno
     *
     * @param string $quoteno
     *
     * @return Orders
     */
    public function setQuoteno($quoteno) {
        $this->quoteno = $quoteno;

        return $this;
    }

    /**
     * Get quoteno
     *
     * @return string
     */
    public function getQuoteno() {
        return $this->quoteno;
    }

    /**
     * Set serviceCost
     *
     * @param string $serviceCost
     *
     * @return Orders
     */
    public function setServiceCost($serviceCost) {
        $this->service_cost = $serviceCost;

        return $this;
    }

    /**
     * Get serviceCost
     *
     * @return string
     */
    public function getServiceCost() {
        return $this->service_cost;
    }

    /**
     * Set travelCost
     *
     * @param string $travelCost
     *
     * @return Orders
     */
    public function setTravelCost($travelCost) {
        $this->travel_cost = $travelCost;

        return $this;
    }

    /**
     * Get travelCost
     *
     * @return string
     */
    public function getTravelCost() {
        return $this->travel_cost;
    }

    /**
     * Set other
     *
     * @param string $other
     *
     * @return Orders
     */
    public function setOther($other) {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return string
     */
    public function getOther() {
        return $this->other;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Orders
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set isInclude
     *
     * @param integer $isInclude
     *
     * @return Orders
     */
    public function setIsInclude($isInclude) {
        $this->is_include = $isInclude;

        return $this;
    }

    /**
     * Get isInclude
     *
     * @return integer
     */
    public function getIsInclude() {
        return $this->is_include;
    }

    /**
     * Set otherText
     *
     * @param string $otherText
     *
     * @return Orders
     */
    public function setOtherText($otherText) {
        $this->other_text = $otherText;

        return $this;
    }

    /**
     * Get otherText
     *
     * @return string
     */
    public function getOtherText() {
        return $this->other_text;
    }

    /**
     * Set manualCost
     *
     * @param string $manualCost
     *
     * @return Orders
     */
    public function setManualCost($manualCost) {
        $this->manual_cost = $manualCost;

        return $this;
    }

    /**
     * Get manualCost
     *
     * @return string
     */
    public function getManualCost() {
        return $this->manual_cost;
    }

    /**
     * Set isManualInclude
     *
     * @param integer $isManualInclude
     *
     * @return Orders
     */
    public function setIsManualInclude($isManualInclude) {
        $this->is_manual_include = $isManualInclude;

        return $this;
    }

    /**
     * Get isManualInclude
     *
     * @return integer
     */
    public function getIsManualInclude() {
        return $this->is_manual_include;
    }


    /**
     * Set subscription
     *
     * @param integer $subscription
     *
     * @return Orders
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription
     *
     * @return integer
     */
    public function getSubscription()
    {
        return $this->subscription;
    }


    /**
     * Set taxstatus
     *
     * @param integer $taxstatus
     *
     * @return Orders
     */
    public function setTaxstatus($taxstatus)
    {
        $this->taxstatus = $taxstatus;

        return $this;
    }

    /**
     * Get taxstatus
     *
     * @return integer
     */
    public function getTaxstatus()
    {
        return $this->taxstatus;
    }

    /**
     * Set specialPrice
     *
     * @param string $specialPrice
     *
     * @return Orders
     */
    public function setSpecialPrice($specialPrice) {
        $this->special_price = $specialPrice;

        return $this;
    }

    /**
     * Get specialPrice
     *
     * @return string
     */
    public function getSpecialPrice() {
        return $this->special_price;
    }
}
