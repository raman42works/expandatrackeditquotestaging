<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsletterMessage
 *
 * @ORM\Table(name="newsletter_message")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\NewsletterMessageRepository")
 */
class NewsletterMessage {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subtext", type="string", length=255, nullable=true)
     */
    private $subtext;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=100, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255, nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $image1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $image2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $image3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $image4;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $document;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $button_text;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $file_url;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() { 
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return NewsletterMessage
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return NewsletterMessage
     */
    public function setText($text) {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * Set image1
     *
     * @param string $image1
     *
     * @return NewsletterMessage
     */
    public function setImage1($image1) {
        $this->image1 = $image1;

        return $this;
    }

    /**
     * Get image1
     *
     * @return string
     */
    public function getImage1() {
        return $this->image1;
    }

    /**
     * Set image2
     *
     * @param string $image2
     *
     * @return NewsletterMessage
     */
    public function setImage2($image2) {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get image2
     *
     * @return string
     */
    public function getImage2() {
        return $this->image2;
    }

    /**
     * Set image3
     *
     * @param string $image3
     *
     * @return NewsletterMessage
     */
    public function setImage3($image3) {
        $this->image3 = $image3;

        return $this;
    }

    /**
     * Get image3
     *
     * @return string
     */
    public function getImage3() {
        return $this->image3;
    }

    /**
     * Set image4
     *
     * @param string $image4
     *
     * @return NewsletterMessage
     */
    public function setImage4($image4) {
        $this->image4 = $image4;

        return $this;
    }

    /**
     * Get image4
     *
     * @return string
     */
    public function getImage4() {
        return $this->image4;
    }


    /**
     * Set subtext
     *
     * @param string $subtext
     *
     * @return NewsletterMessage
     */
    public function setSubtext($subtext)
    {
        $this->subtext = $subtext;

        return $this;
    }

    /**
     * Get subtext
     *
     * @return string
     */
    public function getSubtext()
    {
        return $this->subtext;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return NewsletterMessage
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set document
     *
     * @param string $document
     *
     * @return NewsletterMessage
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set buttonText
     *
     * @param string $buttonText
     *
     * @return NewsletterMessage
     */
    public function setButtonText($buttonText)
    {
        $this->button_text = $buttonText;

        return $this;
    }

    /**
     * Get buttonText
     *
     * @return string
     */
    public function getButtonText()
    {
        return $this->button_text;
    }

    /**
     * Set fileUrl
     *
     * @param string $fileUrl
     *
     * @return NewsletterMessage
     */
    public function setFileUrl($fileUrl)
    {
        $this->file_url = $fileUrl;

        return $this;
    }

    /**
     * Get fileUrl
     *
     * @return string
     */
    public function getFileUrl()
    {
        return $this->file_url;
    }
}
