<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceOrder
 *
 * @ORM\Table(name="service_order")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\ServiceOrderRepository")
 */
class ServiceOrder {

    public function __construct() {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orders", inversedBy="ServiceOrder")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $Orders;

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="ServiceOrder")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $Service;

    /**
     * @ORM\ManyToOne(targetEntity="Variation", inversedBy="ServiceOrder")
     * @ORM\JoinColumn(name="variation_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $Variation;

    /**
     * @ORM\ManyToOne(targetEntity="Installationcost", inversedBy="ServiceOrder")
     * @ORM\JoinColumn(name="installationcost_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $Installationcost;

    /**
     * @var string
     *
     * @ORM\Column(name="meta", type="text")
     */
    private $meta;

    /**
     * @var string
     *
     * @ORM\Column(name="parameter", type="text")
     */
    private $parameter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="service_cost ", type="string")
     */
    private $service_cost;
    
    /**
     * @var string
     *
     * @ORM\Column(name="travel_cost", type="string")
     */
    private $travel_cost;
    
    /**
     * @var string
     *
     * @ORM\Column(name="other ", type="string")
     */
    private $other ;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description ", type="text",nullable=true)
     */
    private $description ;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_text ", type="text",nullable=true)
     */
    private $customer_text ; 
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="qty", type="string")
     */
    private $qty;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set meta
     *
     * @param string $meta
     *
     * @return ServiceOrder
     */
    public function setMeta($meta) {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return string
     */
    public function getMeta() {
        return $this->meta;
    }
    public function getSimpleMeta() {
        $data = unserialize($this->meta);
        $data['parameter'] = unserialize($data["parameter"]);
        return $data;
    }

    /**
     * Set parameter
     *
     * @param string $parameter
     *
     * @return ServiceOrder
     */
    public function setParameter($parameter) {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter
     *
     * @return string
     */
    public function getParameter() {
        return $this->parameter;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ServiceOrder
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ServiceOrder
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set orders
     *
     * @param \ExpandatrckBundle\Entity\Orders $orders
     *
     * @return ServiceOrder
     */
    public function setOrders(\ExpandatrckBundle\Entity\Orders $orders = null) {
        $this->Orders = $orders;

        return $this;
    }

    /**
     * Get orders
     *
     * @return \ExpandatrckBundle\Entity\Orders
     */
    public function getOrders() {
        return $this->Orders;
    }

    /**
     * Set service
     *
     * @param \ExpandatrckBundle\Entity\Service $service
     *
     * @return ServiceOrder
     */
    public function setService(\ExpandatrckBundle\Entity\Service $service = null) {
        $this->Service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \ExpandatrckBundle\Entity\Service
     */
    public function getService() {
        return $this->Service;
    }

    /**
     * Set variation
     *
     * @param \ExpandatrckBundle\Entity\Variation $variation
     *
     * @return ServiceOrder
     */
    public function setVariation(\ExpandatrckBundle\Entity\Variation $variation = null) {
        $this->Variation = $variation;

        return $this;
    }

    /**
     * Get variation
     *
     * @return \ExpandatrckBundle\Entity\Variation
     */
    public function getVariation() {
        return $this->Variation;
    }

    /**
     * Set installationcost
     *
     * @param \ExpandatrckBundle\Entity\Installationcost $installationcost
     *
     * @return ServiceOrder
     */
    public function setInstallationcost(\ExpandatrckBundle\Entity\Installationcost $installationcost = null) {
        $this->Installationcost = $installationcost;

        return $this;
    }

    /**
     * Get installationcost
     *
     * @return \ExpandatrckBundle\Entity\Installationcost
     */
    public function getInstallationcost() {
        return $this->Installationcost;
    }


    /**
     * Set qty
     *
     * @param string $qty
     *
     * @return ServiceOrder
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return string
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set serviceCost
     *
     * @param string $serviceCost
     *
     * @return ServiceOrder
     */
    public function setServiceCost($serviceCost)
    {
        $this->service_cost = $serviceCost;

        return $this;
    }

    /**
     * Get serviceCost
     *
     * @return string
     */
    public function getServiceCost()
    {
        return $this->service_cost;
    }

    /**
     * Set travelCost
     *
     * @param string $travelCost
     *
     * @return ServiceOrder
     */
    public function setTravelCost($travelCost)
    {
        $this->travel_cost = $travelCost;

        return $this;
    }

    /**
     * Get travelCost
     *
     * @return string
     */
    public function getTravelCost()
    {
        return $this->travel_cost;
    }

    /**
     * Set other
     *
     * @param string $other
     *
     * @return ServiceOrder
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return string
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ServiceOrder
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set customerText
     *
     * @param string $customerText
     *
     * @return ServiceOrder
     */
    public function setCustomerText($customerText)
    {
        $this->customer_text = $customerText;

        return $this;
    }

    /**
     * Get customerText
     *
     * @return string
     */
    public function getCustomerText()
    {
        return $this->customer_text;
    }
}
