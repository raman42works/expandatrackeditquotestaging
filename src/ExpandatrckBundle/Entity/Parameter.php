<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parameter
 *
 * @ORM\Table(name="parameter")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\ParameterRepository")
 */
class Parameter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="Parameter")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $Service;
   
    /**
     * @ORM\ManyToOne(targetEntity="Parameter", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id",onDelete = "SET NULL")
     * 
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Parameter", mappedBy="parent", cascade={"remove"})
     * @ORM\OrderBy({"value" = "ASC"})
     */
    private $children;
    
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    
    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="string", length=255,nullable=true)
     */
    private $cost;
    
        /**
     * @var string
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;
     /**
     * @var string
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Parameter
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Constructor
     */
    public function __construct()
    {

        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt= new \DateTime();
    }

    /**
     * Set service
     *
     * @param \ExpandatrckBundle\Entity\Service $service
     *
     * @return Parameter
     */
    public function setService(\ExpandatrckBundle\Entity\Service $service = null)
    {
        $this->Service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \ExpandatrckBundle\Entity\Service
     */
    public function getService()
    {
        return $this->Service;
    }

    /**
     * Add child
     *
     * @param \ExpandatrckBundle\Entity\Parameter $child
     *
     * @return Parameter
     */
    public function addChild(\ExpandatrckBundle\Entity\Parameter $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \ExpandatrckBundle\Entity\Parameter $child
     */
    public function removeChild(\ExpandatrckBundle\Entity\Parameter $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     * 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \ExpandatrckBundle\Entity\Parameter $parent
     *
     * @return Parameter
     */
    public function setParent(\ExpandatrckBundle\Entity\Parameter $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \ExpandatrckBundle\Entity\Parameter
     */
        public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Parameter
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Parameter
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Parameter
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
