<?php

namespace ExpandatrckBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="Orders", mappedBy="User")
     * 
     */
    private $Orders;

     /**
     * @ORM\OneToMany(targetEntity="Settings", mappedBy="User")
     * 
     */
    private $Settings;
    /**
     * Get id
     *
     * @return int
     */
    
    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=255)
     */
    private $fname = "";
    
    /**
     * @var string
     *
     * @ORM\Column(name="lname", type="string", length=255)
     */
    private $lname = "";
    
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone = "";
    
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address = "";
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;
    
     /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text")
     */
    private $bio = "";
    
      /**
     * @var string
     *
     * @ORM\Column(name="emailpwd", type="string", length=255, nullable=true)
     */
    private $emailpwd;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email_send_from", type="string", length=255, nullable=true)
    */
    private $email_send_from;
    
    
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     *
     * @return User
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname
     *
     * @return string
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     *
     * @return User
     */
    public function setLname($lname)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get lname
     *
     * @return string
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return User
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Add order
     *
     * @param \ExpandatrckBundle\Entity\Orders $order
     *
     * @return User
     */
    public function addOrder(\ExpandatrckBundle\Entity\Orders $order)
    {
        $this->Orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \ExpandatrckBundle\Entity\Orders $order
     */
    public function removeOrder(\ExpandatrckBundle\Entity\Orders $order)
    {
        $this->Orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->Orders;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set emailpwd
     *
     * @param string $emailpwd
     *
     * @return User
     */
    public function setEmailpwd($emailpwd)
    {
        $this->emailpwd = $emailpwd;

        return $this;
    }

    /**
     * Get emailpwd
     *
     * @return string
     */
    public function getEmailpwd()
    {
        return $this->emailpwd;
    }

    /**
     * Set emailSendFrom
     *
     * @param string $emailSendFrom
     *
     * @return User
     */
    public function setEmailSendFrom($emailSendFrom)
    {
        $this->email_send_from = $emailSendFrom;

        return $this;
    }

    /**
     * Get emailSendFrom
     *
     * @return string
     */
    public function getEmailSendFrom()
    {
        return $this->email_send_from;
    }

    /**
     * Add setting
     *
     * @param \ExpandatrckBundle\Entity\Settings $setting
     *
     * @return User
     */
    public function addSetting(\ExpandatrckBundle\Entity\Settings $setting)
    {
        $this->Settings[] = $setting;

        return $this;
    }

    /**
     * Remove setting
     *
     * @param \ExpandatrckBundle\Entity\Settings $setting
     */
    public function removeSetting(\ExpandatrckBundle\Entity\Settings $setting)
    {
        $this->Settings->removeElement($setting);
    }

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettings()
    {
        return $this->Settings;
    }
}
