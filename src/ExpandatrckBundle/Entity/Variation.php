<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variation
 *
 * @ORM\Table(name="variation")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\VariationRepository")
 */
class Variation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="Variation")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $Service;
    
    /**

     * @ORM\OneToMany(targetEntity="ServiceOrder", mappedBy="Variation")
     * 
     */
    private $ServiceOrder;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="start_height", type="string", length=255)
     */
    private $startHeight = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="end_height", type="string", length=255)
     */
    private $endHeight = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="start_with", type="string", length=255)
     */
    private $startWith = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="end_with", type="string", length=255)
     */
    private $endWith = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="string", length=255)
     */
    private $cost;
    
    /**
     * @var string
     *
     * @ORM\Column(name="alter_text", type="string", length=255, nullable=true)
     */
    private $alter_text;
    /**
     * @var string
     *
     * @ORM\Column(name="admin_text", type="string", length=255, nullable=true)
     */
    private $admin_text; 
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;


         /**
     * @var string
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;
     /**
     * @var string
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startHeight
     *
     * @param string $startHeight
     *
     * @return Variation
     */
    public function setStartHeight($startHeight)
    {
        $this->startHeight = $startHeight;

        return $this;
    }

    /**
     * Get startHeight
     *
     * @return string
     */
    public function getStartHeight()
    {
        return $this->startHeight;
    }

    /**
     * Set endHeight
     *
     * @param string $endHeight
     *
     * @return Variation
     */
    public function setEndHeight($endHeight)
    {
        $this->endHeight = $endHeight;

        return $this;
    }

    /**
     * Get endHeight
     *
     * @return string
     */
    public function getEndHeight()
    {
        return $this->endHeight;
    }

    /**
     * Set startWith
     *
     * @param string $startWith
     *
     * @return Variation
     */
    public function setStartWith($startWith)
    {
        $this->startWith = $startWith;

        return $this;
    }

    /**
     * Get startWith
     *
     * @return string
     */
    public function getStartWith()
    {
        return $this->startWith;
    }

    /**
     * Set endWith
     *
     * @param string $endWith
     *
     * @return Variation
     */
    public function setEndWith($endWith)
    {
        $this->endWith = $endWith;

        return $this;
    }

    /**
     * Get endWith
     *
     * @return string
     */
    public function getEndWith()
    {
        return $this->endWith;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Variation
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set service
     *
     * @param \ExpandatrckBundle\Entity\Service $service
     *
     * @return Variation
     */
    public function setService(\ExpandatrckBundle\Entity\Service $service = null)
    {
        $this->Service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \ExpandatrckBundle\Entity\Service
     */
    public function getService()
    {
        return $this->Service;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ServiceOrder = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt= new \DateTime();
    }

    /**
     * Add serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     *
     * @return Variation
     */
    public function addServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder)
    {
        $this->ServiceOrder[] = $serviceOrder;

        return $this;
    }

    /**
     * Remove serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     */
    public function removeServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder)
    {
        $this->ServiceOrder->removeElement($serviceOrder);
    }

    /**
     * Get serviceOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceOrder()
    {
        return $this->ServiceOrder;
    }

    /**
     * Set alterText
     *
     * @param string $alterText
     *
     * @return Variation
     */
    public function setAlterText($alterText)
    {
        $this->alter_text = $alterText;

        return $this;
    }

    /**
     * Get alterText
     *
     * @return string
     */
    public function getAlterText()
    {
        return $this->alter_text;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Variation
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set adminText
     *
     * @param string $adminText
     *
     * @return Variation
     */
    public function setAdminText($adminText)
    {
        $this->admin_text = $adminText;

        return $this;
    }

    /**
     * Get adminText
     *
     * @return string
     */
    public function getAdminText()
    {
        return $this->admin_text;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Variation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Variation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
