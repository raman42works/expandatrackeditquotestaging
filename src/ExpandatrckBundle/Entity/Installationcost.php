<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Installationcost
 *
 * @ORM\Table(name="installationcost")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\InstallationcostRepository")
 */
class Installationcost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="Installationcost")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id",onDelete = "SET NULL")
     */
    private $Service;
    
    /**

     * @ORM\OneToMany(targetEntity="ServiceOrder", mappedBy="Installationcost")
     * 
     */
    private $ServiceOrder;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="start_range", type="string", length=255)
     */
    private $startRange;

    /**
     * @var string
     *
     * @ORM\Column(name="end_range", type="string", length=255)
     */
    private $endRange;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="string", length=255)
     */
    private $cost;


      /**
     * @var string
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;
     /**
     * @var string
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startRange
     *
     * @param string $startRange
     *
     * @return Installationcost
     */
    public function setStartRange($startRange)
    {
        $this->startRange = $startRange;

        return $this;
    }

    /**
     * Get startRange
     *
     * @return string
     */
    public function getStartRange()
    {
        return $this->startRange;
    }

    /**
     * Set endRange
     *
     * @param string $endRange
     *
     * @return Installationcost
     */
    public function setEndRange($endRange)
    {
        $this->endRange = $endRange;

        return $this;
    }

    /**
     * Get endRange
     *
     * @return string
     */
    public function getEndRange()
    {
        return $this->endRange;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Installationcost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set service
     *
     * @param \ExpandatrckBundle\Entity\Service $service
     *
     * @return Installationcost
     */
    public function setService(\ExpandatrckBundle\Entity\Service $service = null)
    {
        $this->Service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \ExpandatrckBundle\Entity\Service
     */
    public function getService()
    {
        return $this->Service;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ServiceOrder = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt= new \DateTime();
    }

    /**
     * Add serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     *
     * @return Installationcost
     */
    public function addServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder)
    {
        $this->ServiceOrder[] = $serviceOrder;

        return $this;
    }

    /**
     * Remove serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     */
    public function removeServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder)
    {
        $this->ServiceOrder->removeElement($serviceOrder);
    }

    /**
     * Get serviceOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceOrder()
    {
        return $this->ServiceOrder;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Installationcost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Installationcost
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
