<?php

namespace ExpandatrckBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="ExpandatrckBundle\Repository\ServiceRepository")
 */
class Service
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**

     * @ORM\OneToMany(targetEntity="Variation", mappedBy="Service")
     * 
     */
    private $Variation;
    
    /**

     * @ORM\OneToMany(targetEntity="ServiceOrder", mappedBy="Service")
     * 
     */
    private $ServiceOrder;
    
    
    /**

     * @ORM\OneToMany(targetEntity="Installationcost", mappedBy="Service")
     * 
     */
    private $Installationcost;
    
    /**
     * @ORM\ManyToOne(targetEntity="Orders", inversedBy="Service")
     * 
     */
    private $Orders;
    
     /**
     * @ORM\OneToMany(targetEntity="Parameter", mappedBy="Service")
     */
    private $Parameter;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="service_name", type="string", length=255)
     */
    private $serviceName;

    /**
     * @var string
     *
     * @ORM\Column(name="cost_type", type="string", length=255)
     */
    private $costType;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="string", length=255)
     */
    private $cost;

     /**
     * @var string
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;
     /**
     * @var string
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     *
     * @return Service
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Set costType
     *
     * @param string $costType
     *
     * @return Service
     */
    public function setCostType($costType)
    {
        $this->costType = $costType;

        return $this;
    }

    /**
     * Get costType
     *
     * @return string
     */
    public function getCostType()
    {
        return $this->costType;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return Service
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Variation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Installationcost = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Parameter = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt= new \DateTime();
       // $this->updatedAt= new \DateTime();
    }

    /**
     * Add variation
     *
     * @param \ExpandatrckBundle\Entity\Variation $variation
     *
     * @return Service
     */
    public function addVariation(\ExpandatrckBundle\Entity\Variation $variation)
    {
        $this->Variation[] = $variation;

        return $this;
    }

    /**
     * Remove variation
     *
     * @param \ExpandatrckBundle\Entity\Variation $variation
     */
    public function removeVariation(\ExpandatrckBundle\Entity\Variation $variation)
    {
        $this->Variation->removeElement($variation);
    }

    /**
     * Get variation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVariation()
    {
        return $this->Variation;
    }

    /**
     * Add installationcost
     *
     * @param \ExpandatrckBundle\Entity\Installationcost $installationcost
     *
     * @return Service
     */
    public function addInstallationcost(\ExpandatrckBundle\Entity\Installationcost $installationcost)
    {
        $this->Installationcost[] = $installationcost;

        return $this;
    }

    /**
     * Remove installationcost
     *
     * @param \ExpandatrckBundle\Entity\Installationcost $installationcost
     */
    public function removeInstallationcost(\ExpandatrckBundle\Entity\Installationcost $installationcost)
    {
        $this->Installationcost->removeElement($installationcost);
    }

    /**
     * Get installationcost
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstallationcost()
    {
        return $this->Installationcost;
    }

    /**
     * Set orders
     *
     * @param \ExpandatrckBundle\Entity\Orders $orders
     *
     * @return Service
     */
    public function setOrders(\ExpandatrckBundle\Entity\Orders $orders = null)
    {
        $this->Orders = $orders;

        return $this;
    }

    /**
     * Get orders
     *
     * @return \ExpandatrckBundle\Entity\Orders
     */
    public function getOrders()
    {
        return $this->Orders;
    }

    /**
     * Add parameter
     *
     * @param \ExpandatrckBundle\Entity\Parameter $parameter
     *
     * @return Service
     */
    public function addParameter(\ExpandatrckBundle\Entity\Parameter $parameter)
    {
        $this->Parameter[] = $parameter;

        return $this;
    }

    /**
     * Remove parameter
     *
     * @param \ExpandatrckBundle\Entity\Parameter $parameter
     */
    public function removeParameter(\ExpandatrckBundle\Entity\Parameter $parameter)
    {
        $this->Parameter->removeElement($parameter);
    }

    /**
     * Get parameter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParameter()
    {
        return $this->Parameter;
    }

    /**
     * Add serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     *
     * @return Service
     */
    public function addServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder)
    {
        $this->ServiceOrder[] = $serviceOrder;

        return $this;
    }

    /**
     * Remove serviceOrder
     *
     * @param \ExpandatrckBundle\Entity\ServiceOrder $serviceOrder
     */
    public function removeServiceOrder(\ExpandatrckBundle\Entity\ServiceOrder $serviceOrder)
    {
        $this->ServiceOrder->removeElement($serviceOrder);
    }

    /**
     * Get serviceOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceOrder()
    {
        return $this->ServiceOrder;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Service
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Service
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
