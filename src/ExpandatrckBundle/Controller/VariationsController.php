<?php

namespace ExpandatrckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\Variation;
use Symfony\Component\Filesystem\Filesystem;
class VariationsController extends Controller {

    /**
     * @Route("/service/{id}/variations",name="variations")
     * @Template()
     */
    public function indexAction(Request $request,$id) {

        $em = $this->getDoctrine()->getManager();
        
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        } 
        $variation = $service->getVariation();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $variation, $request->query->getInt('page', ($request->get('page')) ? $request->get('page') : 1)/* page number */, 10/* limit per page */
        );
        $allservice = $em->getRepository('ExpandatrckBundle:Service')->findAll();


        return array(
            'entities' => $pagination,
            'page_title' => 'Variations',
            'service_id' => $id,
            'services'=>$allservice,
            'pagination' => $pagination,
            'servicename'=>$service->getServiceName()
        );

    }

    /**
     * @Route("service/update_variations_cost1",name="update_variations_cost1")
     * 
     * @Template()
     */
    public function updateCostAction(Request $request) {
        
        $form_data = $request->request->all();
        //print_r($form_data);die;


        $serviceid = $form_data['serviceid'];
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }


        $RAW_QUERY = "SELECT * FROM variation where service_id= $serviceid";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $variations = $statement->fetchAll();

        if(!empty($variations)){

            foreach($variations as $vr){
                $variation_id = $vr['id'];
                $ventity = $em->getRepository('ExpandatrckBundle:Variation')->find($variation_id);
                //var_dump($ventity);die;
                $originalCost =  $ventity->getcost(); 

                if(!empty($originalCost)){
                    if($form_data['amount_type']=='fixed'){
                        if($form_data['add_subtract_symbol']=='+'){
                            $originalCost = intval($originalCost) + intval($form_data['add_subtract_amount']);
                        }else{
                            $originalCost = intval($originalCost) - intval($form_data['add_subtract_amount']);
                        }
                    }else{
                        $percentage  = $form_data['add_subtract_amount'];
                        $percentage_amount  = ($originalCost * $percentage)/100;
                        if($form_data['add_subtract_symbol']=='+'){


                            $originalCost = intval($originalCost) + intval($percentage_amount);
                        }else{
                            $originalCost = intval($originalCost) - intval($percentage_amount);
                        }
                    }
                }
               
                $ventity->setCost($originalCost);
                $em = $this->getDoctrine()->getManager();
                $em->persist($ventity);
                $em->flush(); 
            }
        }
        
      

        /*$entity = new Service();
        $form_data = $request->request->all();
        
        $em = $this->getDoctrine()->getManager();
        
        $entity->setServiceName($form_data['servicename']);
        $entity->setCostType($form_data['costtype']);
        $entity->setCost($form_data['cost']);
        $em->persist($entity);
        
        $variation = new \ExpandatrckBundle\Entity\Variation();
        $variation->setCost($form_data['cost']);
        $variation->setService($entity);
        
        $em->persist($variation);
      
        $installtioncost = new \ExpandatrckBundle\Entity\Installationcost;
        
        $installtioncost->setCost(0);
        $installtioncost->setStartRange(0);
        $installtioncost->setEndRange(0);
        $installtioncost->setCost(0);
        $installtioncost->setService($entity);
        $em->persist($installtioncost);
        
        $em->flush();
        
        return $this->redirect($this->generateUrl('service'));*/

        return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
    }
     /**
     * @Route("service/upload_csv",name="upload_csv")
     * 
     * @Template()
     */
    public function uploadCsvAction(Request $request) {

        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];

        $em = $this->getDoctrine()->getManager();
        
        
        if(isset($_FILES["variarion_csv"])) { 

            $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
            if(!in_array($_FILES['variarion_csv']['type'],$mimes)){
               $this->get('session')
                ->getFlashBag()
                ->add('error', 'Please upload a csv file!');
                 return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
            }

            $csv=file_get_contents($_FILES["variarion_csv"]["tmp_name"]);
            $data = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $csv));
            $data = array_splice($data,1,count($data));

           
            //saving variations
            if(!empty($data)){
                foreach($data as $d){
                    if(!empty($d[1])){

                        $entity = new Variation();
                      
                        $service = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

                        if (!$service) {
                            throw $this->createNotFoundException('Unable to find Service entity.');
                        }

                        $file = $request->files->get('variation_image');
                        
                        /*if (!empty($file)) {
                           
                            $fs = new Filesystem();
                            try {
                                
                                if (!file_exists("uploads/variation/")) {
                                     
                                    $fs->mkdir("uploads/variation");
                                    
                                }
                            } catch (IOExceptionInterface $e) {
                                echo "An error occurred while creating your directory at " . $e->getPath();
                            }
                            $filename = time() . $file->getClientOriginalName();
                            $file->move("uploads/variation/", $filename);
                            $entity->setPath($filename);
                        }
                        */
                        $entity->setStartHeight($d[0]);
                        $entity->setEndHeight($d[1]);
                        $entity->setStartWith($d[2]);
                        $entity->setEndWith($d[3]);
                        $entity->setCost($d[4]);
                        if(!empty($d[5])){
                            $entity->setPath($d[5]);
                        }
                        //$entity->setAlterText($form_data['alter']);
                        $entity->setService($service);

                        $em->persist($entity);
                        $em->flush();
                    }
                }
            }

        }
    
            $this->get('session')
                ->getFlashBag()
                ->add('success', 'Variations uploaded successfully!');
        return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
    }
      /**
     * @Route("service/upload_images",name="upload_images")
     * 
     * @Template()
     */
    public function uploadImagesAction(Request $request) {

        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];

        $em = $this->getDoctrine()->getManager();
        
        
        if($_FILES["zip_file"]["name"]) {
            $filename = $_FILES["zip_file"]["name"];
            $source = $_FILES["zip_file"]["tmp_name"];
            $type = $_FILES["zip_file"]["type"];
            
            $name = explode(".", $filename);
            $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
            foreach($accepted_types as $mime_type) {
                if($mime_type == $type) {
                    $okay = true;
                    break;
                } 
            }
            
            $fs = new Filesystem();
            try {
                
                if (!file_exists("uploads/temp/")) { 
                     
                    $fs->mkdir("uploads/temp");
                    
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }

            $continue = strtolower($name[1]) == 'zip' ? true : false;
            if(!$continue) {
                
                   $this->get('session')
                ->getFlashBag()
                ->add('error', 'Please upload a zip file!.');
                return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
            }

           $target_path = "uploads/temp/".$filename;  // change this to the correct site path

            if(move_uploaded_file($source, $target_path)) { 

                $zip = new \ZipArchive();

                $x = $zip->open($target_path);
                if ($x === true) {

                    $zip->extractTo("uploads/variation/".$serviceid.'/'); // change this to the correct site path
                    $zip->close();
            
                    unlink($target_path);
                }
                
                $this->get('session') 
                ->getFlashBag()
                ->add('success', 'Your .zip file was uploaded!');
            } else {    
               
                 $this->get('session')
                ->getFlashBag()
                ->add('error', 'There was a problem with the upload. Please try again.');
                return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
            }
        }
    
            $this->get('session')
                ->getFlashBag()
                ->add('success', 'Images uploaded successfully!');
        return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
    }
     /**
     * @Route("/variations/delete_all",name="delete_all_variations")
     * 
     * @Template()
     */
    public function deleteAllAction(Request $request) {

        $form_data = $request->request->all();
        $variations = $form_data['variarions'];
        //print_r($variations);die;
        foreach($variations as $v){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ExpandatrckBundle:Variation')->find($v);
            $em->remove($entity);
            $em->flush();
            
        }
        echo 1;die;
        
    }
     /**
     * @Route("/variations/delete_all_one_time",name="delete_all_variations_one_time")
     * 
     * @Template()
     */
    public function deleteAllOneTomeAction(Request $request) {

      $em = $this->getDoctrine()->getManager();

        $form_data = $request->request->all();


        $serviceid = $form_data['service_id'];

        $RAW_QUERY = "SELECT * FROM variation where service_id= $serviceid";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $variations = $statement->fetchAll();

        if(!empty($variations)){

            foreach($variations as $v){
               
                $entity = $em->getRepository('ExpandatrckBundle:Variation')->find($v['id']);
                $em->remove($entity);
                $em->flush();
               
            }
        } 

         echo 1;die;
        
        
    }

    /**
     * @Route("/add/variations/",name="add_variations")
     * 
     * @Template()
     */
    public function addAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $entity = new Variation();
        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];

        $service = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $file = $request->files->get('variation_image');
        
        if (!empty($file)) {
           
            $fs = new Filesystem();
            try {
                
                if (!file_exists("uploads/variation/".$serviceid.'/')) {
                     
                    $fs->mkdir("uploads/variation/".$serviceid.'/');
                    
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }
            $filename = time() . $file->getClientOriginalName();
            $file->move("uploads/variation/".$serviceid.'/', $filename);
            $entity->setPath($filename);
        }
        $entity->setStartHeight($form_data['start_height']);
        $entity->setEndHeight($form_data['end_height']);
        $entity->setStartWith($form_data['start_with']);
        $entity->setEndWith($form_data['end_with']);
        $entity->setCost($form_data['cost']);
        $entity->setAlterText($form_data['alter']);
        $entity->setService($service);

        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('variations', array('id' => $serviceid)));
    }
    
    /**
     * @Route("/edit/variations/",name="edit_variations")
     * 
     * @Template()
     */
    public function editAction(Request $request) {
         $form_data = $request->request->all();
         $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Variation')->find($form_data['variationid']);
        $page_number = ($form_data['pagenumber']) ? $form_data['pagenumber'] : 1; 
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Variation entity.');
        } 
        
        $file = $request->files->get('variation_image');
        
        if (!empty($file)) {
           
            $fs = new Filesystem();
            try {
                
                if (!file_exists("uploads/variation/".$serviceid.'/')) {
                     
                    $fs->mkdir("uploads/variation/".$serviceid.'/');
                    
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }
            $filename = time() . $file->getClientOriginalName();
            $file->move("uploads/variation/".$serviceid.'/', $filename);
            $entity->setPath($filename);
        }
        
        $entity->setStartHeight($form_data['start_height']);
        $entity->setEndHeight($form_data['end_height']);
        $entity->setStartWith($form_data['start_with']);
        $entity->setEndWith($form_data['end_with']);
        $entity->setCost($form_data['cost']);
        $entity->setAlterText($form_data['alter']);
        $entity->setAdminText($form_data['admin_text']); 
        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('variations',array('id'=>$form_data['serviceid'],'page'=>$page_number)));
    }
    
    /**
     * @Route("/delete/variations/{id}/{serviceid}",name="delete_variations")
     * @Template()
     */
    public function deleteAction(Request $request, $id,$serviceid) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Variation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Variation entity.');
        }
       try {
            $em->remove($entity);
            $em->flush();
        } catch (\Exception $e) {
            $request->getSession()
          ->getFlashBag()
          ->add('error', 'Installation can not delete!')
         ;
       }
        return $this->redirect($this->generateUrl('variations',array('id'=>$serviceid)));
    }

}
