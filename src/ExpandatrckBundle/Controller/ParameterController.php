<?php

namespace ExpandatrckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\Parameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ParameterController extends Controller {

    /**
     * @Route("/service/{id}/parameter",name="parameter")
     * @Template()
     */
    public function indexAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();

        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $parameters = $service->getParameter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $parameters, $request->query->getInt('page', 1)/* page number */, 5/* limit per page */
        );
        $allservice = $em->getRepository('ExpandatrckBundle:Service')->findAll();
        return array(
            'entities' => $pagination,
            'service_id' => $id,
            'page_title' => 'Parameter',
            'services'=>$allservice,
        );
    }

    /**
     * @Route("/add/parameter/",name="add_parameter")
     * 
     * @Template()
     */
    public function addAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Parameter();
        $form_data = $request->request->all();
        
        //print_r($form_data); die;
        $serviceid = $form_data['serviceid'];

        $service = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $entity->setValue($form_data['parametername']);
        $entity->setService($service);

        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('parameter', array('id' => $serviceid)));
    }

    /**
     * @Route("/delete/parameter/{id}/{serviceid}",name="delete_parameter")
     * @Template()
     */
    public function deleteAction(Request $request, $id, $serviceid) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Parameter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parameter entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
        } catch (\Exception $e) {
            $request->getSession()
          ->getFlashBag()
          ->add('error', 'Installation can not delete!')
         ;
       }
        return $this->redirect($this->generateUrl('parameter', array('id' => $serviceid)));
    }

    /**
     * @Route("/service/{serviceid}/parameter/{id}",name="parameter_inner")
     * 
     * @Template()
     */
    public function parameterinnerAction(Request $request, $id, $serviceid) {
        
        
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('ExpandatrckBundle:Parameter')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Parameter entity.');
        }
        $childs = $service->getChildren();
       
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $childs, $request->query->getInt('page', ($request->get('page')) ? $request->get('page') : 1 )/* page number */, 10/* limit per page */
        );
        $allservice = $em->getRepository('ExpandatrckBundle:Service')->findAll();
        $parameters = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$parameters) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $parameters = $parameters->getParameter();
        
        return array(
            'entities' => $pagination,
            'page_title' => 'Parameter Inner',
            'service_id' => $serviceid,
            'parameterid' => $id,
            'services'=>$allservice,
            'parameters'=>$parameters   
            
        );
    }

    /**
     * @Route("/add/inner/parameter/",name="add_inner_parameter")
     * 
     * @Template()
     */
    public function addinnerAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Parameter();
        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];
        $parameterid = $form_data['parameterid'];

        $service = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $parameter = $em->getRepository('ExpandatrckBundle:Parameter')->find($parameterid);

        if (!$parameterid) {
            throw $this->createNotFoundException('Unable to zcxc find Parameter entity.');
        }
        
        $entity->setValue($form_data['parametername']);
        
        $entity->setParent($parameter);
        $entity->setCost($form_data['cost']);
        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('parameter_inner', array('serviceid' => $serviceid,'id'=>$parameterid)));
    }
    
    /**
     * @Route("/delete/inner/parameter/{id}/{serviceid}",name="delete_inner_parameter")
     * @Template()
     */
    public function deleteinnerparameterAction(Request $request, $id, $serviceid) {
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Parameter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parameter entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('parameter', array('id' => $serviceid)));
    }
    
    /**
     * @Route("/get/service/parameter", name="get_service_parameter")
     * 
     * 
     */
    public function getServiceParameter(Request $request){
        
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $parameters = $service->getParameter();
        
        $html = ''; 
        $html .= "<option value=>Please select an option </option>";
        foreach($parameters as $val){
           $url = $this->generateUrl('parameter_inner', array('serviceid' => $id,'id'=>$val->getId()),true);
           $html .= "<option value=$url>".$val->getValue()."</option>";
        }
        echo $html;die;
    }
    /**
     * @Route("/get/service/parameteroptions", name="get_service_parameter_options")
     * 
     * 
     */
    public function getServiceParameterOptions(Request $request){
        
        $id = $request->get('parameter_id'); 
        $em = $this->getDoctrine()->getManager();
        $parameters = $em->getRepository('ExpandatrckBundle:Parameter')->findByParent($id);

        
        $html = ''; 
        $html .= "<select name='parameter_options' onChange='getParameterCost(this.value)'><option value=>Please select an option </option>"; 
        foreach($parameters as $val){ 
           
           $html .= "<option value=".$val->getId().">".$val->getValue()."</option>";
        }
        $html .= '</select>'; 
        echo $html;die; 
    }
    /**
     * @Route("/get/service/parametercost", name="get_service_parameter_cost")
     * 
     * 
     */
    public function getServiceParameterCost(Request $request){ 
        
        $id = $request->get('parameter_id'); 
        $em = $this->getDoctrine()->getManager();
        $parameter = $em->getRepository('ExpandatrckBundle:Parameter')->find($id);

        
        $html = '<input type="text" name="parameter_cost" value='.$parameter->getCost().' />'; 
        
        echo $html;die;  
    }
    
    /**
     * @Route("edit/parameter/inner/cost",name="edit_parameter_inner_cost")
     * @Template()
     */
      public function editParameterCostAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();  
        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];
        $parameterid = $form_data['parameterid']; 
        $parameterinnerid = $form_data['parameterinnerid']; 
        $page_number = ($form_data['pagenumber']) ? $form_data['pagenumber'] : 1; 
       
       
        $entity = $em->getRepository('ExpandatrckBundle:Parameter')->find($parameterinnerid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parameter entity.');
        }
        
        $entity->setCost($form_data['cost']);
        $entity->setValue($form_data['parametername']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('parameter_inner', array('serviceid' => $serviceid,'id'=>$parameterid,'page'=>$page_number)));
      }
}
