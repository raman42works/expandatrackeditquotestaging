<?php

namespace ExpandatrckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ServiceController extends Controller {

    /**
     * @Route("/services",name="service")
     * 
     * @Template()
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $q = $request->get('q');

        if (isset($q)) {
            $entities = $em->getRepository('ExpandatrckBundle:Service')->createQueryBuilder('s')
                    ->where('s.serviceName LIKE :name')
                    ->setParameter('name', "$q%")
                    ->getQuery()
                    ->getResult();
        } else {

            $entities = $em->getRepository('ExpandatrckBundle:Service')->findBy(array(), array('serviceName' => 'ASC'));
        }



//        $paginator = $this->get('knp_paginator');
//        $pagination = $paginator->paginate(
//                $entities, $request->query->getInt('page', 1)/* page number */, 10/* limit per page */
//        );
        return array(
            'entities' => $entities,
            'page_title' => 'Service'
        );
    }

    /**
     * @Route("/add/service/",name="add_service")
     * 
     * @Template()
     */
    public function addAction(Request $request) {

        $entity = new Service();
        $form_data = $request->request->all();
        
        $em = $this->getDoctrine()->getManager();
        
        $entity->setServiceName($form_data['servicename']);
        $entity->setCostType($form_data['costtype']);
        $entity->setCost($form_data['cost']);
        $em->persist($entity);
        
        $variation = new \ExpandatrckBundle\Entity\Variation();
        $variation->setCost($form_data['cost']);
        $variation->setService($entity);
        
        $em->persist($variation);
      
        $installtioncost = new \ExpandatrckBundle\Entity\Installationcost;
        
        $installtioncost->setCost(0);
        $installtioncost->setStartRange(0);
        $installtioncost->setEndRange(0);
        $installtioncost->setCost(0);
        $installtioncost->setService($entity);
        $em->persist($installtioncost);
        
        $em->flush();
        
        return $this->redirect($this->generateUrl('service'));
    }

    /**
     * @Route("/add/service/to/order/",name="add_service_to_order")
     * 
     */
    public function addServiceToOrderAction(Request $request) {

        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $parameters = $service->getParameter();
        $html = '';
        $i = 0;
        $html .="<input type=hidden name=serviceid value=$id />";
        foreach ($parameters as $val) {
            $parameter = $em->getRepository('ExpandatrckBundle:Parameter')->find($val->getId());

            if (!$parameter) {
                throw $this->createNotFoundException('Unable to find Parameter entity.');
            }
            $parameters_list = $parameter->getChildren();
            $html .="<div class=col-sm-6 col-xs-12><div  class=input-group>";
            if (count($parameters_list)) {
                $html .= "<label>" . $val->getValue() . "</label>";
                $html .="<select name=parameterlist[]>";
                foreach ($parameters_list as $value) {

                    $html .= "<option value='" . $value->getValue()."~".$value->getCost() ."'>" . $value->getValue() . "</option>";
                }
            }
            $html .="</select>";
            $html .="</div></div>";
        }
        echo $html; die;
    }
    /**
     * @Route("/update/service/to/order/",name="update_service_to_order")
     * 
     */
    public function updateServiceToOrderAction(Request $request) {

        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $orderservice = $em->getRepository('ExpandatrckBundle:ServiceOrder')->find($id);

        if (!$orderservice) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $service = $em->getRepository('ExpandatrckBundle:Service')->find($orderservice->getService()->getId());

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $meta = $orderservice->getSimpleMeta();
        $parameters = $service->getParameter();
        $html = '';
        $i = 0;
        $html .="<input type=hidden name=serviceid value=".$service->getId()." />";
        $html .="<input type=hidden name=orderserviceid value=".$id." />";
        
        foreach ($parameters as $val) {
            $parameter = $em->getRepository('ExpandatrckBundle:Parameter')->find($val->getId());

            if (!$parameter) {
                throw $this->createNotFoundException('Unable to find Parameter entity.');
            }
            $parameters_list = $parameter->getChildren();
            $html .="<div class=col-sm-6 col-xs-12><div  class=input-group>";
            if (count($parameters_list)) {
                $html .= "<label>" . $val->getValue() . "</label>";
                $html .="<select name=parameterlist[]>";
                foreach ($parameters_list as $value) {
                    $selected = '';

                    if(!empty($meta["parameter"])){

                        if($meta["parameter"][$i]==$value->getValue()."~".$value->getCost()){

                            $selected = "selected='selected'";
                        }
                    }
                    $html .= "<option ".$selected." value='" . $value->getValue()."~".$value->getCost() ."'>" . $value->getValue() . "</option>";
                }
            }
            $html .="</select>";
            $html .="</div></div>";
            $i++;
        }


        echo $html;
        die;
    }

    /**
     * @Route("/edit/service/cost",name="edit_service_cost")
     * 
     * @Template()
     */
    public function editServiceCostAction(Request $request) {
        
        $id = $request->get('serviceid');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $form_data = $request->request->all();
        $entity->setServiceName($form_data['servicename']);
        $entity->setCostType($form_data['costtype']);
        $entity->setCost($form_data['cost']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        
        $installationcost = $em->getRepository('ExpandatrckBundle:Installationcost')->findOneBy(array('Service' => $id));
        $installationcost->setCost($form_data['cost']);
        $em->persist($installationcost);
        
        $em->flush();
        return $this->redirect($this->generateUrl('service'));
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/delete/service/{id}", name="delete_service")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id) {
       
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        try {
            $em->remove($entity);
            $em->flush();
        } catch (\Exception $e) {
            $request->getSession()
          ->getFlashBag()
          ->add('error', 'This Prodcuts already added to orders!')
         ;
       }
        return $this->redirect($this->generateUrl('service'));
    }

    /**
     * @Route("/edit/service/",name="edit_service")
     * 
     * @Template()
     */
    public function editAction(Request $request) {
        $id = $request->get('serviceid');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $form_data = $request->request->all();
        $entity->setServiceName($form_data['servicename']);
        $entity->setCostType($form_data['costtype']);
        $entity->setCost($form_data['cost']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        //update parameter cost
        if($request->get('parameter_options')){
            $id = $request->get('parameter_options');
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ExpandatrckBundle:Parameter')->find($id);
            $entity->setCost($request->get('parameter_cost'));
            $em->persist($entity);
            $em->flush(); 
        }
        
        return $this->redirect($this->generateUrl('service'));
    }

}
