<?php

namespace ExpandatrckBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\Installationcost;

class InstallationController extends Controller
{
    /**
     * @Route("/service/{id}/installation",name="installation")
     * @Template()
     */
    public function indexAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        } 
        $installationcost = $service->getInstallationcost();
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $installationcost, $request->query->getInt('page', ($request->get('page')) ? $request->get('page') : 1)/* page number */, 8/* limit per page */
        );
        $allservice = $em->getRepository('ExpandatrckBundle:Service')->findAll();
        return array(
            'entities' => $pagination,
            'page_title' => 'Installationcost',
            'services'=>$allservice,
            'service_id'=>$id
        );
    }
    
    /**
     * @Route("/add/installationcost/",name="add_installationcost")
     * 
     * @Template()
     */
    public function addAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Installationcost();
        $form_data = $request->request->all();
        $serviceid = $form_data['serviceid'];
        
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($serviceid);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        } 
        
        $entity->setStartRange($form_data['start_range']);
        $entity->setEndRange($form_data['end_range']);
        $entity->setCost($form_data['cost']);
        $entity->setService($service);
        
        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('installation',array('id'=>$serviceid)));
    }
    
    /**
     * @Route("/edit/installationcost/",name="edit_installationcost")
     * 
     * @Template()
     */
    public function editAction(Request $request) {
         $form_data = $request->request->all();
         $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Installationcost')->find($form_data['userid']);
        $page_number = ($form_data['pagenumber']) ? $form_data['pagenumber'] : 1; 
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        } 
        
        $entity->setStartRange($form_data['start_range']);
        $entity->setEndRange($form_data['end_range']);
        $entity->setCost($form_data['cost']);
       
        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('installation',array('id'=>$form_data['serviceid'],'page'=>$page_number)));
    }
    
    /**
     * @Route("/delete/installationcost/{id}/{serviceid}",name="delete_installationcost")
     * @Template()
     */
    public function deleteAction(Request $request, $id,$serviceid) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Installationcost')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
       
        try {
            $em->remove($entity);
            $em->flush();
        } catch (\Exception $e) {
            $request->getSession()
          ->getFlashBag()
          ->add('error', 'Installation can not delete!')
         ;
       }
        return $this->redirect($this->generateUrl('installation',array('id'=>$serviceid)));
    }
    
    /**
     * @Route("/edit/installation/cost",name="edit_installation_cost")
     *
     * @Template()
     */
    
    public function editinstallationAction(Request $request){
        $id = $request->get('serviceid');
        $installationid = $request->get('installationid');
        $em = $this->getDoctrine()->getManager();
        $form_data = $request->request->all();
        $service = $em->getRepository('ExpandatrckBundle:Service')->find($id);

        if (!$service) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }
        $installationcost_entity = $em->getRepository('ExpandatrckBundle:Installationcost')->find($installationid);
         //\Doctrine\Common\Util\Debug::dump($installationcost_entity); die;
        if (!$installationcost_entity) {
            $strt_range = !empty($form_data['start_range']) ? : 0;
            $end_range = !empty($form_data['end_range']) ? : 0;
            $entity = new Installationcost();
            $entity->setStartRange($strt_range);
            $entity->setEndRange($end_range);
            $entity->setCost($form_data['cost']);
            $entity->setService($service);

            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('service'));
        } else {
           
            $installationcost_entity->setStartRange(!empty($form_data['start_range']) ? : 0);
            $installationcost_entity->setEndRange(!empty($form_data['end_range']) ? : 0);
            $installationcost_entity->setCost($form_data['cost']);
            $installationcost_entity->setService($service);

            $em->persist($installationcost_entity);
            $em->flush();
            return $this->redirect($this->generateUrl('service'));
        }
        
    }

}
