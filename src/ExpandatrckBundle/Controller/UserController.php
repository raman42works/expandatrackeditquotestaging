<?php

namespace ExpandatrckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ExpandatrckBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Filesystem\Filesystem;
use ExpandatrckBundle\Entity\Settings;
use ExpandatrckBundle\Entity\Message;

class UserController extends Controller {

    /**
     * @Route("/" ,name="user")
     * 
     * @Template()
     */
    public function indexAction(Request $request) {




        if (!$this->getUser()) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }


        $em = $this->getDoctrine()->getManager();

        $q = $request->get('q');

        if (isset($q)) {
            $dql = "SELECT a FROM ExpandatrckBundle:User a WHERE a.enabled=1 AND (a.fname LIKE '" . $q . "%' OR a.email LIKE '" . $q . "%')  AND  a.id != " . $this->getUser()->getId() . " ORDER BY a.id DESC";
            $entities = $em->createQuery($dql);
            $users = $em->getRepository('ExpandatrckBundle:User')->findAll(array('name' => $q));
        } else {
            $dql = "SELECT a FROM ExpandatrckBundle:User a WHERE a.enabled=1 AND a.id != " . $this->getUser()->getId() . " ORDER BY a.id DESC";
            $entities = $em->createQuery($dql);
            $users = $em->getRepository('ExpandatrckBundle:User')->findAll();
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $request->query->getInt('page', 1)/* page number */, 20/* limit per page */
        );
        return array(
            'entities' => $pagination,
            'page_title' => 'Our Team'
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/user/create", name="user_create")
     * @Method("POST")
     * 
     */
    public function createAction(Request $request) {
        $form_data = $request->request->all();
        $entity = new User();
        $user_email = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:User')
                ->findOneBy(array('email' => $form_data['email']));
        if ($user_email != NULL) {
            $response = new Response("0,Email already exist with us.Please try another email.");
            return $response;
        }
        $user_phone = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:User')
                ->findOneBy(array('phone' => $form_data['phone']));
        if ($user_phone != NULL) {

            $response = new Response("0,Phone number already exist with us.Please try another Phone number.");
            return $response;
        }
        $file = $request->files->get('user_image');

        if (!empty($file)) {

            $fs = new Filesystem();
            try {

                if (!file_exists("uploads/users/")) {
                    $fs->mkdir("uploads/users");
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }
            $filename = time() . $file->getClientOriginalName();
            $file->move("uploads/users/", $filename);
            $entity->setPath($filename);
        }
        $role = 'ROLE_STAFF';


        $username = substr($form_data['email'], 0, strpos($form_data['email'], '@'));
        $entity->setEnabled(1);
        $entity->setUsername($username);

        $entity->setFname($form_data['fname']);
        $entity->setLname($form_data['lname']);
        $entity->setEmail($form_data['email']);

        $entity->setPhone($form_data['phone']);
        $entity->setAddress($form_data['address']);
        $entity->setBio($form_data['bio']);
        $entity->setRoles(array());
        $entity->addRole($role);
        $entity->setPlainPassword($form_data['password']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $message = \Swift_Message::newInstance()
                ->setSubject('Registration Successful')
                ->setFrom('admin@ars.com')
                ->setTo($form_data['email'])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/email.html.twig
                        'email.html.twig', array('name' => $username, 'email' => $form_data['email'], 'password' => $form_data['password'])
                ), 'text/html'
        );
        $this->get('session')->getFlashBag()->add(
                'success', 'User Created.'
        );
        $this->get('mailer')->send($message);
        $response = new Response("1,User Created Successfully.");
        return $response;
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/delete/{id}", name="user_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/user/edit", name="user_edit")
     * @Method("POST")
     * 
     */
    public function editAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new User();
        $form_data = $request->request->all();

        $entity = $em->getRepository('ExpandatrckBundle:User')->find($form_data['userid']);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $file = $request->files->get('user_image');

        if (!empty($file)) {

            $fs = new Filesystem();
            try {

                if (!file_exists("uploads/users/")) {
                    $fs->mkdir("uploads/users");
                }
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating your directory at " . $e->getPath();
            }
            $filename = time() . $file->getClientOriginalName();
            $file->move("uploads/users/", $filename);
            $entity->setPath($filename);
        }


        $entity->setEnabled(1);
        $entity->setUsername($form_data['fname']);
        $entity->setUsernameCanonical($form_data['fname']);
        $entity->setFname($form_data['fname']);
        $entity->setLname($form_data['lname']);
        $entity->setPhone($form_data['phone']);
        $entity->setAddress($form_data['address']);
        $entity->setBio($form_data['bio']);
        $entity->setPlainPassword($form_data['password']);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * 
     *
     * @Route("/user/setting", name="user_settings")
     * @Template()
     * 
     */
    public function settingsAction(Request $request) {
        $session = $request->getSession();
        $login_user_id = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
//        $entity = $this->getDoctrine()
//                ->getRepository('ExpandatrckBundle:User')
//                ->find($login_user_id);
        $RAW_QUERY = "SELECT *  FROM  settings where user_id = $login_user_id";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $unique_users = array();
        $entity = $statement->fetch();
        //print_r($entity);die;
        return array(
            'entity' => $entity,
            'page_title' => 'Settings'
        );
    }

    /**
     * @Route("/save/settings", name="setting_save")
     * @Method("POST")
     * 
     */
    function saveSettingAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $form_data = $request->request->all();
        $entity = new Settings();

        $login_user_id = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('ExpandatrckBundle:User')->find($login_user_id);

        
        $RAW_QUERY = "SELECT * FROM settings   where user_id= $login_user_id";

        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();

        $check_record = $statement->fetchAll();

        //print_r($check_record);die;

        if ($check_record != NULL) {

           // $check_record = $this->getDoctrine()->getRepository('ExpandatrckBundle:Settings')->findOneBy(array('UserId' => $login_user_id));
            
            

            $email = $form_data['email'];
            $sql = "UPDATE settings  SET email='$email' WHERE user_id = $login_user_id ";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();

            return $this->redirect($this->generateUrl('user_settings'));
        } else {
            $check_record = new Settings();  
            $check_record->setEmail($form_data['email']);
            $check_record->setUser($user);
            $check_record->setCreatedAt(new \DateTime('now'));
            $check_record->setUpdatedAt(new \DateTime('now'));
            //$check_record->setEmailpwd($form_data['emailpwd']);
//            $entity->setCreatedAt(new \DateTime());
//            $entity->setUpdatedAt(new \DateTime());
            $em->persist($check_record);
            $em->flush();
            return $this->redirect($this->generateUrl('user_settings')); 
        }
    }

    /**
     * @Route("/user/message", name="user_message")
     * @Template()
     * 
     */
    public function messageAction(Request $request) {

        $entity = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:Message')
                ->find(1);

        return array(
            'entity' => $entity,
            'page_title' => 'Settings'
        );
    }

    /**
     * @Route("/save/message", name="msg_save")
     * @Method("POST")
     * 
     */
    function saveMessageAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $form_data = $request->request->all();
        $entity = new Message();
        $check_record = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:Message')
                ->find(1);
        if ($check_record != NULL) {

            $check_record->setText($form_data['msg']);

            $em->persist($check_record);

            $em->flush();
            return $this->redirect($this->generateUrl('user_message'));
        } else {

            $entity->setText($form_data['msg']);
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('user_message'));
        }
    }

}
