<?php

namespace ExpandatrckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\ServiceOrder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ServiceOrderController extends Controller {

    /**
     * @Route("/order/{id}/add/service/",name="add_order_service")
     * @Template()
     */
    public function indexAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $q = $request->get('q');

        if (isset($q)) {
            $entities = $em->getRepository('ExpandatrckBundle:Service')->createQueryBuilder('s')
                    ->where('s.serviceName LIKE :name')
                    ->setParameter('name', "$q%")
                    ->getQuery()
                    ->getResult();
        } else {

            $entities = $em->getRepository('ExpandatrckBundle:Service')->findBy(array(), array('serviceName' => 'ASC'));
        }

        return array(
            'entities' => $entities,
            'page_title' => 'Add Service',
            'orderid' => $id
        );
    }

    /**
     * @Route("/add/service/order",name="add_service_order")
     * 
     * @Template()
     */
    public function addOrderServiceAction(Request $request) {
      
        $em = $this->getDoctrine()->getManager();
        $service_id = $request->get('serviceid');
        $orderid = $request->get('orderid');
        $height = ($request->get('height')) ? $request->get('height') : 0;
        $width = ($request->get('width')) ? $request->get('width') : 0;
        $parameterlist = $request->get('parameterlist');

      
        $check_fixed_service = $em->getRepository('ExpandatrckBundle:Service')->findOneBy(array('id' => $service_id, 'costType' => 'fixedcost'));
       
        if ($check_fixed_service) {
            
            $parameter_cost = array();
            if(!empty($parameterlist)){
                foreach($parameterlist as $val){
                    $para_arr = explode('~',$val);
                   
                    $parameter_cost[] = array($para_arr[0]=>$para_arr[1]);
                }
            }
            
            $meta = array('parameter' => serialize($parameterlist));
            $variation = $em->getRepository('ExpandatrckBundle:Variation')->findOneBy(array('Service' => $service_id));
            $installationCost = $em->getRepository('ExpandatrckBundle:Installationcost')->findOneBy(array('Service' => $service_id));
           
        } else {
            $parameter_cost = array();
            if(!empty($parameterlist)){
                foreach($parameterlist as $val){
                    $para_arr = explode('~',$val);
                   
                    $parameter_cost[] = array($para_arr[0]=>$para_arr[1]);
                }
            }
              
            $meta = array('Width' => $width, 'Height' => $height,'parameter' => serialize($parameterlist));
            $check_fixed_service = $em->getRepository('ExpandatrckBundle:Service')->findOneBy(array('id' => $service_id));
            
             $sql = "SELECT * FROM `variation` "
                    . "WHERE ($height between `start_height` and `end_height` ) "
                    . "and ($width between `start_with` and `end_with` ) and service_id = $service_id order by id DESC limit 1";
           
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();

            $variationId = $stmt->fetchColumn(0);
            $variation = $em->getRepository('ExpandatrckBundle:Variation')->findOneBy(array('id' => $variationId));
            
            
            $sql1 = "SELECT * FROM installationcost  WHERE ($width between `start_range` and `end_range` ) and service_id = $service_id order by id DESC limit 1";
            $stmt1 = $em->getConnection()->prepare($sql1);
            $stmt1->execute();
            $installationCostId = $stmt1->fetchColumn(0);
            $installationCost = $em->getRepository('ExpandatrckBundle:Installationcost')->findOneBy(array('id' => $installationCostId));
            

            if (!$variation || !$installationCost) {
                $this->get('session')
                        ->getFlashBag()
                        ->add('error', "Error: No Variation/Installation-cost found for this height:$height and width:$width.");
                return $this->redirect($this->generateUrl('list_service_to_order', array('id' => $orderid)));
            }
        }

       

        $order = $em->getRepository('ExpandatrckBundle:Orders')->find($orderid);

        if (!$order) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

       
        if (!$variation) {
            throw $this->createNotFoundException('Unable to find Variation entity.');
        }

        
        $entity = new ServiceOrder();
        
        $entity->setMeta(serialize($meta));
        $entity->setParameter(serialize($parameter_cost));
        $entity->setOrders($order);
        $entity->setVariation($variation);
        $entity->setService($check_fixed_service);
        $entity->setDescription($request->get('description'));
        $entity->setCustomerText($request->get('customer_text')); 
        $entity->setQty(1);
        $entity->setServiceCost(0);
        $entity->setTravelCost(0);
        $entity->setOther(0);
        $entity->setInstallationcost($installationCost);
        
        $em->persist($entity);
        $em->flush();

        
        return $this->redirect($this->generateUrl('list_service_to_order', array('id' => $orderid)));
    }
    /**
     * @Route("/update/service/order",name="update_service_order")
     * 
     * @Template()
     */
    public function updateOrderServiceAction(Request $request) {
      
        $em = $this->getDoctrine()->getManager();
        $service_id = $request->get('serviceid');
        $orderid = $request->get('orderid');
        $height = ($request->get('height')) ? $request->get('height') : 0;
        $width = ($request->get('width')) ? $request->get('width') : 0;
        $parameterlist = $request->get('parameterlist');

      
        $check_fixed_service = $em->getRepository('ExpandatrckBundle:Service')->findOneBy(array('id' => $service_id, 'costType' => 'fixedcost'));
       
        if ($check_fixed_service) {
            
            $parameter_cost = array();
            if(!empty($parameterlist)){
                foreach($parameterlist as $val){
                    $para_arr = explode('~',$val);
                   
                    $parameter_cost[] = array($para_arr[0]=>$para_arr[1]);
                }
            }
            
            $meta = array('parameter' => serialize($parameterlist));
            $variation = $em->getRepository('ExpandatrckBundle:Variation')->findOneBy(array('Service' => $service_id));
            $installationCost = $em->getRepository('ExpandatrckBundle:Installationcost')->findOneBy(array('Service' => $service_id));
           
        } else {
            $parameter_cost = array();
            if(!empty($parameterlist)){
                foreach($parameterlist as $val){
                    $para_arr = explode('~',$val);
                   
                    $parameter_cost[] = array($para_arr[0]=>$para_arr[1]);
                }
            }
              
            $meta = array('Width' => $width, 'Height' => $height,'parameter' => serialize($parameterlist));
            $check_fixed_service = $em->getRepository('ExpandatrckBundle:Service')->findOneBy(array('id' => $service_id));
            
             $sql = "SELECT * FROM `variation` "
                    . "WHERE ($height between `start_height` and `end_height` ) "
                    . "and ($width between `start_with` and `end_with` ) and service_id = $service_id order by id DESC limit 1";
           
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();

            $variationId = $stmt->fetchColumn(0);
            $variation = $em->getRepository('ExpandatrckBundle:Variation')->findOneBy(array('id' => $variationId));
            
            
            $sql1 = "SELECT * FROM installationcost  WHERE ($width between `start_range` and `end_range` ) and service_id = $service_id order by id DESC limit 1";
            $stmt1 = $em->getConnection()->prepare($sql1);
            $stmt1->execute();
            $installationCostId = $stmt1->fetchColumn(0);
            $installationCost = $em->getRepository('ExpandatrckBundle:Installationcost')->findOneBy(array('id' => $installationCostId));
            

            if (!$variation || !$installationCost) {
                $this->get('session')
                        ->getFlashBag()
                        ->add('error', "Error: No Variation/Installation-cost found for this height:$height and width:$width.");
                return $this->redirect($this->generateUrl('list_service_to_order', array('id' => $orderid)));
            }
        }

       

        $order = $em->getRepository('ExpandatrckBundle:Orders')->find($orderid);

        if (!$order) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

       
        if (!$variation) {
            throw $this->createNotFoundException('Unable to find Variation entity.');
        }

        
        
         $em = $this->getDoctrine()->getManager();

         //$entity = new ServiceOrder();
        
    $entity = $em
        ->getRepository('ExpandatrckBundle:ServiceOrder')
        ->find($request->get("orderserviceid"));

        
        $entity->setMeta(serialize($meta));
        $entity->setParameter(serialize($parameter_cost));
        $entity->setOrders($order);
        $entity->setVariation($variation);
        $entity->setService($check_fixed_service);
        $entity->setDescription($request->get('description'));
        $entity->setCustomerText($request->get('customer_text')); 
        //$entity->setQty(1);
        //$entity->setServiceCost(0);
        ///$entity->setTravelCost(0);
        //$entity->setOther(0);
        //$entity->setInstallationcost($installationCost);
        
        $em->persist($entity);
        $em->flush();

        
        return $this->redirect($this->generateUrl('edit_order', array('id' => $orderid)));
    }
    /**
     * @Route("/add/qty/",name="add_qty")
     * @Template()
     */
    public function sendquotionAction(Request $request) {

        $id = $request->get('id');
        $qty = $request->get('qty');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:ServiceOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $entity->setQty($qty);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Saved quantity");
        return $response;
    }

    

    /**
     * @Route("/delete/service/to/{id}/order/{orderid}",name="delete_service_to_order")
     * @Template()
     */
    public function deleteAction(Request $request, $id, $orderid) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:ServiceOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ServiceOrder entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('edit_order', array('id' => $orderid)));
    }
    
    /**
     * @Route("/add/desc",name="add_desc")
     * @Template()
     */
    public function addDesAction(Request $request) {

        $id = $request->get('id');
        $othercost = $request->get('description');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:ServiceOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $entity->setDescription($othercost);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Saved Description");
        return $response;
    }

}
