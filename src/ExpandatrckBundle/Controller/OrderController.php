<?php

namespace ExpandatrckBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use ExpandatrckBundle\Entity\Orders;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Swift_Attachment;
use Swift_Image;
use ExpandatrckBundle\Entity\Settings;
use ExpandatrckBundle\Entity\Message;
use Symfony\Component\Filesystem\Filesystem;
class OrderController extends Controller {

    /**
     * @Route("/order",name="order")
     * 
     * @Template()
     */
    public function indexAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $q = $request->get('q');

        $order_status = $request->get('order_status');

        if(isset($order_status)){
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($request->get('order'));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

           
            $entity->setTaxstatus($order_status);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            unset($_GET['order']); 
            unset($_GET['order_status']); 

            $qs = http_build_query($_GET);
//print_r("/order?"+$qs); die;
           /* $referer = $this->getRequest()
                ->headers
                ->get('referer');*/

                return $this->redirect("/order?".$qs);

        }

        if (isset($q)) {

            $dql = "SELECT o FROM ExpandatrckBundle:Orders o WHERE  (o.name LIKE '" . $q . "%' OR o.email LIKE '" . $q . "%')  ORDER BY o.id DESC";
            $entities = $em->createQuery($dql);
        } else {
           $dql = "SELECT o FROM ExpandatrckBundle:Orders o WHERE  (o.name LIKE '" . $q . "%' OR o.email LIKE '" . $q . "%')  ORDER BY o.id DESC";
            $entities = $em->createQuery($dql);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $request->query->getInt('page', 1)/* page number */, 10/* limit per page */
        );


        return array(
            'entities' => $pagination,
            'page_title' => 'Orders',
        );
    }

    /**
     * @Route("/order/add/",name="add_order")
     * 
     * @Template()
     */
    public function addAction(Request $request) {
        return array(
            'page_title' => 'Add Order',
        );
    }

    /**
     * @Route("/order/create/",name="create_order")
     * 
     * @Template()
     */
    public function createAction(Request $request) {

        $form_data = $request->request->all();
        $user = $this->getUser();
        $entity = new Orders();


        $quoteDate = $request->get('quote_date');
        $setQuoteDate = new \DateTime($quoteDate);

        $expiresDate = $request->get('expires');
        $setExpiresDate = new \DateTime($expiresDate);
        $set_quoteno = mt_rand(10000000, 99999999);
        $form_data = $request->request->all();
        $entity->setName($form_data['name']);
        $entity->setEmail($form_data['email']);
        $entity->setPhone($form_data['phone']);
        $entity->setAddress($form_data['address']);
        $entity->setQuoteDate($setQuoteDate);
        $entity->setExpires($setExpiresDate);
        $entity->setQuoteno($set_quoteno);
        $entity->setQuotation($form_data['quotation']);
        $entity->setTaxstatus(1);
        $entity->setTaxstatus($form_data['tax_status']);
        $entity->setServiceCost(0);
        $entity->setTravelCost(0);
        $entity->setOther(0);
        $entity->setSubscription(1);
        $entity->setUser($user);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $order_id = $entity->getId();


        //add user to mailchimp ////////////////////////////////////////////////////
        $flname = explode(' ',$form_data['name']);
        $fname = $flname[0];
        $lname = @$flname[1];

        $apiKey = 'e830fd22ce1ccf4ec9518679828350bf-us18';
        $listId = '2fb92971c7';

        $json = json_encode([
            'email_address' => $form_data['email'],
            'update_existing'=>true,
            'status'        => 'subscribed', // "subscribed","unsubscribed","cleaned","pending"
            'merge_fields'  => [
                'FNAME'     => $fname,
                /*'LNAME'     => $lname*/
            ]
        ]);

        try{                   
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "https://us18.api.mailchimp.com/3.0/lists/".$listId."/members");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_USERPWD, "anystring" . ":" . $apiKey);

                $headers = array();
                $headers[] = "Content-Type: application/x-www-form-urlencoded";
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $result = curl_exec($ch);

                //var_dump($result);die;

                if (curl_errno($ch)) { 
                    echo 'Error:' . curl_error($ch);
                }
                curl_close ($ch);
        }catch(\Exception $e){
            
        }


        $this->get('session')
                ->getFlashBag()
                ->add('success', '* Order Saved,Now you can add the service with this order.!');
        //$response = new Response($order_id);
        //return $response;
        return $this->redirect($this->generateUrl('list_service_to_order', array('id' => $order_id)));
    }

    /**
     * @Route("/order/edit/{id}",name="edit_order")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id ASC";
        $order_details = $em->createQuery($dql)->getResult();
        if ($order_details != null) {
            return array(
                'entity' => $entity,
                'orderdetails' => $order_details,
                'id' => $order_details[0]->getId(),
                'service_cost' => $order_details[0]->getServiceCost(),
                'travel_cost' => $order_details[0]->getTravelCost(),
                'other' => $order_details[0]->getOther(),
                'page_title' => 'Edit Order',
            );
        } else {
            return array(
                'entity' => $entity,
                'orderdetails' => $order_details,
                'page_title' => 'Edit Order',
            );
        }
    }

    /**
     * @Route("/delete/order/{id}",name="delete_order")
     * @Template()
     */
    public function deleteAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('order'));
    }

    /**
     * @Route("/update/order/",name="update_order")
     * @Template()
     */
    public function updateAction(Request $request) {
        $form_data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($form_data['orderid']);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $user = $this->getUser();
        $quoteDate = $request->get('quote_date');
        $setQuoteDate = new \DateTime($quoteDate);

        $expiresDate = $request->get('expires');
        $setExpiresDate = new \DateTime($expiresDate);

        $form_data = $request->request->all();
        $entity->setName($form_data['name']);
        $entity->setEmail($form_data['email']);
        $entity->setPhone($form_data['phone']);
        $entity->setAddress($form_data['address']);
        $entity->setTaxstatus($form_data['tax_status']);
        $entity->setQuoteDate($setQuoteDate);
        $entity->setExpires($setExpiresDate);
        $entity->setQuotation($form_data['quotation']);
        
        $entity->setUser($user);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('order'));
    }

    /**
     * @Route("/send/quotion/{id}",name="send_quotion")
     * @Template()
     */
    public function sendquotionAction(Request $request, $id) {

        $result_array = array();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        $groupsql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id GROUP BY so.description ORDER BY so.id DESC";
        $groupsql_res = $em->createQuery($groupsql)->getResult();
        foreach($groupsql_res as $val){
             $desc = $val->getDescription();
             //$dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id  AND so.description = '".$desc."' ORDER BY so.id DESC"; 
             if(!empty($desc) && $desc != NULL){
                
             $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id  AND so.description = '".$desc."' ORDER BY so.id DESC"; 
             } else {
               
                $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id  AND so.description =''  ORDER BY so.id DESC"; 
             }
             $order_detail = $em->createQuery($dql)->getResult();
            $result_array[] = array(
                                'key'=>$val->getDescription(),
                                'value'=>$order_detail
            );
        }
        
        
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id DESC";
        $order_details = $em->createQuery($dql)->getResult();
        //\Doctrine\Common\Util\Debug::dump($order_details); die;
       //return $this->render('admin.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details));
        
        
        
     //return $this->render('admin.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details));
        
        $admin_pdf = time(). '.pdf';
        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('admin.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details)), 'uploads/templatepdf/' . $admin_pdf
        );

        $message = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:Message')
                ->find(1);
            return array(
                'entity' => $entity,
                'orderdetails' => $order_details,
                'page_title' => 'Quotion',
                'result' => $result_array,
                'pdf'=>$admin_pdf,
                'message'=>$message
            );
       
    }

    /**
     * @Route("/send/invoice/{id}",name="send_invoice")
     * @Template()
     */
    public function sendinvoiceAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id GROUP BY so.Service ORDER BY so.id DESC";
        $order_details = $em->createQuery($dql)->getResult();
        
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id DESC";
        $invoice_details = $em->createQuery($dql)->getResult();
        // \Doctrine\Common\Util\Debug::dump($entity->getEmail()); 
        
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id DESC";
        $invoice_details = $em->createQuery($dql)->getResult();
        
        
        
        
        $admin_pdf = mt_rand(10000, 999999) . '.pdf';
        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('invoice.html.twig', array('invoice_details'=>$invoice_details,'entity' => $entity, 'orderdetails' => $order_details)), 'uploads/templatepdf/' . $admin_pdf
        );
        if ($order_details != null) {
            return array(
                'entity' => $entity,
                'orderdetails' => $order_details,
                'page_title' => 'Invoice',
                'id' => $order_details[0]->getId(),
                'service_cost' => $order_details[0]->getServiceCost(),
                'travel_cost' => $order_details[0]->getTravelCost(),
                'other' => $order_details[0]->getOther(),
                'invoice_details'=>$invoice_details,
                'pdf'=>$admin_pdf
            );
        } else {
            return array(
                'entity' => $entity,
                'orderdetails' => $order_details,
                'page_title' => 'Invoice',
                'pdf'=>$admin_pdf
            );
        }

//        // Force the download
//        header("Content-Disposition: attachment; filename=" . basename($file) );
//        header("Content-Length: " . filesize($file));
//        header("Content-Type: application/pdf;");
//        readfile($file);
    }

    /**
     * @Route("/order/{id}/details/",name="list_service_to_order")
     * 
     * @Template()
     */
    public function orderservicelistAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);
       // echo 1;die;
        /*if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        */


         //$order_details = $this->getDoctrine()
             //   ->getRepository('ExpandatrckBundle:ServiceOrder')
               // ->find($id);

        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id ASC";
        $order_details = $em->createQuery($dql)->getResult();

        if ($order_details != null) {
        return array(
            'entity' => $entity,
            'orderdetails' => $order_details,
            'id' => $order_details[0]->getId(),
            'service_cost' => $order_details[0]->getServiceCost(),
            'travel_cost' => $order_details[0]->getTravelCost(),
            'other' => $order_details[0]->getOther(),
            'page_title' => 'Order Service List',
        );
        } else {
          return array(
            'entity' => $entity,
           
            'page_title' => 'Order Service List',
        );  
        }
    }

    /**
     * @Route("/send/quotation/email/{id}",name="send_quotation_email")
     * 
     * @Template()
     */
    public function sendQuotationEmail(Request $request, $id) {
      
        $result_array = array();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $groupsql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id GROUP BY so.description ORDER BY so.id DESC";
        $groupsql_res = $em->createQuery($groupsql)->getResult();
        foreach($groupsql_res as $val){
             $desc = $val->getDescription(); 
             $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id  AND so.description = '".$desc."' ORDER BY so.id DESC";
             $order_detail = $em->createQuery($dql)->getResult();
            $result_array[] = array(
                                'key'=>$val->getDescription(),
                                'value'=>$order_detail
            );
        }
        
        
        
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id DESC";
        $order_details = $em->createQuery($dql)->getResult();
       
        $filename = mt_rand(10000, 999999) . '.pdf';
       // $this->render('quotation.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther()));

        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('quotation.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())), 'uploads/templatepdf/' . $filename
        );
        
        $admin_pdf = time(). '.pdf';
        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('admin.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())), 'uploads/templatepdf/' . $admin_pdf
        );
        
         
        //$login_user = $this->container->get('security.context')->getToken()->getUser();
        $login_user_id = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $userentity = $em->getRepository('ExpandatrckBundle:User')->find($login_user_id);
        $loggedInEmail = $userentity->getEmail();

        $RAW_QUERY = "SELECT * FROM settings   where user_id= $login_user_id";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $settings_email = $statement->fetch();
        
        if($settings_email['email'] != ""){
            $settings_email = $settings_email['email'];
        } else {
            $settings_email = $loggedInEmail;
        }

       

        $message = \Swift_Message::newInstance()
                ->setSubject('Your Order Quotation: ' . $entity->getQuoteno())
                ->setFrom(array($settings_email => $settings_email))
                ->setReplyTo(array($settings_email))
                ->setTo($entity->getEmail())
                ->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $filename))
                ->attach(Swift_Attachment::fromPath('uploads/terms&condition/Expandatrack_Terms_conditions.pdf'))
                ->setBody(
                $this->renderView(
                        // app/Resources/views/email.html.twig
                        'quotation.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())
                ), 'text/html'
        );
        $this->get('mailer')->send($message);

        $admin_email = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:Settings')
                ->find(1);

        if($admin_email->getEmail() != ""){
            $adminEmail = $admin_email->getEmail(); 
        } else {
            //$adminEmail = "expandatrack@gmail.com";
            $adminEmail = "expandatrack@gmail.com";
        }

        //$adminEmail = "expandatrack@gmail.com"; 
        $adminmessage = \Swift_Message::newInstance()
                ->setSubject('Your Order Quotation: ' . $entity->getQuoteno())
                ->setFrom(array($adminEmail => $adminEmail))
                ->setReplyTo(array($adminEmail)) 
                ->setTo($adminEmail)
                ->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $admin_pdf))
                ->setBody(
                $this->renderView(
                        // app/Resources/views/email.html.twig
                        'admin.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())
                ), 'text/html'
        );
        $this->get('mailer')->send($adminmessage);

         $this->get('session')
                ->getFlashBag()
                ->add('success', 'Mail Sent Successfully');

        return $this->redirect($this->generateUrl('order'));
    }

    /**
     * @Route("/send/invoice/email/{id}",name="send_invoice_email")
     * 
     * @Template()
     */
    public function sendInvoiceEmail(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id GROUP BY so.Service ORDER BY so.id DESC";
        $order_details = $em->createQuery($dql)->getResult();
        // \Doctrine\Common\Util\Debug::dump($order_details[0]->getServiceCost()); die;
        
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id DESC";
        $invoice_details = $em->createQuery($dql)->getResult();
        
        
        
        $filename = mt_rand(10000, 999999) . '.pdf';
        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('invoice.html.twig', array('invoice_details'=>$invoice_details,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())), 'uploads/templatepdf/' . $filename
        );
        
        //$login_user = $this->container->get('security.context')->getToken()->getUser();
        $login_user_id = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $userentity = $em->getRepository('ExpandatrckBundle:User')->find($login_user_id);
        $loggedInEmail = $userentity->getEmail();

        $RAW_QUERY = "SELECT * FROM settings   where user_id= $login_user_id";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $settings_email = $statement->fetch();
        
        if($settings_email['email'] != ""){
            $settings_email = $settings_email['email'];
        } else {
            $settings_email = $loggedInEmail;
        }
        
        $message = \Swift_Message::newInstance()
                ->setSubject('Your Order Invoice: ' . $entity->getQuoteno())
                ->setFrom(array($settings_email => $settings_email))
                ->setTo($entity->getEmail())
                ->setReplyTo(array($settings_email)) 
                ->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $filename))
                ->setBody(
                $this->renderView(
                        // app/Resources/views/email.html.twig
                        'invoice.html.twig', array('invoice_details'=>$invoice_details,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())
                ), 'text/html'
        );
        
        $adminEmail1 = "expandatrack@gmail.com";
        $admin_email = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:Settings')
                ->find(1);
         if($admin_email->getEmail() != ""){
             $adminEmail1 = $admin_email->getEmail();
        } else {
             $adminEmail1 = "expandatrack@gmail.com";
        }
        //echo $adminEmail1;die;
        $adminmessage = \Swift_Message::newInstance()
                ->setSubject('Your Order Invoice: ' . $entity->getQuoteno())
                ->setFrom(array($adminEmail1 => $adminEmail1))
                ->setTo($adminEmail1)
                ->setReplyTo(array($adminEmail1)) 
                ->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $filename))
                ->setBody(
                $this->renderView(
                        // app/Resources/views/email.html.twig
                        'invoice.html.twig', array('invoice_details'=>$invoice_details,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())
                ), 'text/html'
        );
        $this->get('mailer')->send($message);
        $this->get('mailer')->send($adminmessage);

         $this->get('session')
                ->getFlashBag()
                ->add('success', 'Mail Sent Successfully');


        return $this->redirect($this->generateUrl('order'));
    }
    
    /**
     * @Route("/add/service/cost",name="add_service_cost")
     * @Template()
     */
      public function addServiceCostAction(Request $request) {
          
        $id = $request->get('id'); 
        $servicecost = $request->get('servicecost');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $entity->setServiceCost($servicecost);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Saved Service Cost");
        return $response;
      }
      
     /**
     * @Route("/add/travel/cost",name="add_travel_cost")
     * @Template()
     */
      public function addTravelCostAction(Request $request) {
          
        $id = $request->get('id');
        $travelcost = $request->get('servicecost');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $entity->setTravelCost($travelcost);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Saved Travel Cost");
        return $response;
      }
      
     /**
     * @Route("/add/other/cost",name="add_other_cost")
     * @Template()
     */
      public function addOtherCostAction(Request $request) {
          
        $id = $request->get('id');
        $othercost = $request->get('othercost');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $entity->setOther($othercost);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Saved Other Cost");
        return $response;
      }
      
      /**
     * @Route("/order/include/installation/cost",name="include_cost")
     * @Template()
     */
      public function includeCostAction(Request $request) {
          
        $id = $request->get('id');
        $include = $request->get('include');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $entity->setIsInclude($include);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Included cost");
        return $response;
      }
      
      /**
     * @Route("/add/other/text",name="add_other_text")
     * @Template()
     */
      public function addOtherTextAction(Request $request) {
          
        $id = $request->get('id');
        $othertext = $request->get('othertext');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $entity->setOtherText($othertext);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("Saved Other Text");
        return $response;
      }
      
      /**
     * @Route("/add/manual/cost",name="add_manual_cost")
     * @Template()
     */
      public function addManualAction(Request $request) {
          
        $id = $request->get('id');
        $cost = $request->get('cost');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $entity->setManualCost($cost);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $response = new Response("manual cost saved");
        return $response;
      }
      
      /**
     * @Route("/order/include/manual/cost",name="include_manual_cost")
     * @Template()
     */
      public function includeManualCostAction(Request $request) {
          
            $id = $request->get('id');
            $include = $request->get('include');
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            $entity->setIsManualInclude($include);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $response = new Response("Included cost");
            return $response;
      }
      
      /**
     * @Route("/order/special-price/add",name="add_special_price")
     * @Template()
     */
      public function addSpecialPriceAction(Request $request) {
          
            $id = $request->get('id');
            $price = $request->get('price');
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            $entity->setSpecialPrice($price);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $response = new Response("Included Special cost");
            return $response;
      }
      
    /**
     * @Route("/order/send/manual/email",name="send_manual_email")
     * @Template()
     */
      
      public function sendManualEmail(Request $request) {
       
        $id = $request->get('orderid');
        
        $result_array = array();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ExpandatrckBundle:Orders')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }
        
        $groupsql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id GROUP BY so.description ORDER BY so.id DESC";
        $groupsql_res = $em->createQuery($groupsql)->getResult();
        foreach($groupsql_res as $val){
             $desc = $val->getDescription(); 
             $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id  AND so.description = '".$desc."' ORDER BY so.id DESC";
             $order_detail = $em->createQuery($dql)->getResult();
            $result_array[] = array(
                                'key'=>$val->getDescription(),
                                'value'=>$order_detail
            );
        }
        
         $file = $request->files->get('user_image');
           $exterattachment  = array();
          
        if (count($file) > 0) {
           
            $fs = new Filesystem();
            foreach($file as $val){
                if(!empty($val)){
                    try {

                        if (!file_exists("uploads/extraattachment/")) {
                            $fs->mkdir("uploads/extraattachment");
                        }
                    } catch (IOExceptionInterface $e) {
                        echo "An error occurred while creating your directory at " . $e->getPath();
                    }
                    $imagename = time() . $val->getClientOriginalName();
                    $val->move("uploads/extraattachment/", $imagename);
                    $exterattachment[] =  $imagename;
                }
            }
        }
        
        
        $dql = "SELECT so FROM ExpandatrckBundle:ServiceOrder so WHERE so.Orders = $id ORDER BY so.id DESC";
        $order_details = $em->createQuery($dql)->getResult();
       
       // echo '<pre>';
        //var_dump($order_detail[0]->getMeta());die;

        $filename = mt_rand(10000, 999999) . '.pdf';
       // $this->render('quotation.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther()));

        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('quotation.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())), 'uploads/templatepdf/' . $filename
        );
        
       $subject = ($request->get('subject')) ? $request->get('subject').': '.$entity->getQuoteno(): 'Your Order Quotation: ' . $entity->getQuoteno(); 
       $to_email =  ($request->get('to_email')) ? : $entity->getEmail();
       
        $login_user_id = $this->getUser()->getId();
               $em = $this->getDoctrine()->getManager();
        $userentity = $em->getRepository('ExpandatrckBundle:User')->find($login_user_id);
        $loggedInEmail = $userentity->getEmail();

        $RAW_QUERY = "SELECT * FROM settings   where user_id= $login_user_id";
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $settings_email = $statement->fetch();

       if($settings_email['email'] != ""){
            $settings_email = $settings_email['email'];
        } else {
            $settings_email = $loggedInEmail;
        }

       // echo $settings_email;die;
        if(count($file) > 0){

                    $message = \Swift_Message::newInstance();
                    $message->setSubject($subject);
                    $message->setFrom(array($settings_email=>$settings_email));
                    $message->setTo($to_email);
                    $message->setReplyTo(array($settings_email));
                    $message->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $filename));
                    $message->attach(Swift_Attachment::fromPath('uploads/terms&condition/Expandatrack_Terms_conditions.pdf'));
                    foreach($exterattachment as $value){
                     $message->attach(Swift_Attachment::fromPath('uploads/extraattachment/' . $value));
                    }
                    $message->setBody($request->get('msg'),'text/html');
            $this->get('mailer')->send($message);
        } else {
            //$to_email = 'ankit.chugh@42works.net';
            $message = \Swift_Message::newInstance()
                    ->setContentType("text/html")
                    ->setSubject($subject)
                    ->setReplyTo(array($settings_email))
                    ->setFrom(array($settings_email=>$settings_email))
                    ->setTo($to_email)
                    ->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $filename))
                    ->attach(Swift_Attachment::fromPath('uploads/terms&condition/Expandatrack_Terms_conditions.pdf'))
                    
                    ->setBody($request->get('msg'),'text/html');
            $a = $this->get('mailer')->send($message); 
            //print_r($a);die; 
        }
        
        $adminEmail1 = "expandatrack@gmail.com";
        $admin_email = $this->getDoctrine()
                ->getRepository('ExpandatrckBundle:Settings')
                ->find(1);
         if($admin_email->getEmail() != ""){
            $adminEmail1 = $admin_email->getEmail();
        } else {
            $adminEmail1 = "expandatrack@gmail.com";
        }
        //echo $adminEmail1;die; 
        //$adminEmail1 = "nishant@yopmail.com";
        $admin_pdf = time(). '.pdf';
        $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('admin.html.twig', array('result'=>$result_array,'entity' => $entity, 'orderdetails' => $order_details, 'service_cost' => $order_details[0]->getServiceCost(), 'travel_cost' => $order_details[0]->getTravelCost(), 'other' => $order_details[0]->getOther())), 'uploads/templatepdf/' . $admin_pdf
        );
        // Send Admin Email
        if(count($file) > 0 && !empty($file[0])){
            $adminmessage = \Swift_Message::newInstance();
                    $adminmessage->setContentType("text/html");
                    $adminmessage->setSubject($subject);
                    $adminmessage->setFrom(array($adminEmail1=>$adminEmail1));
                    $adminmessage->setReplyTo(array($adminEmail1));
                    $adminmessage->setTo($adminEmail1);
                    $adminmessage->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $admin_pdf));
                    $adminmessage->attach(Swift_Attachment::fromPath('uploads/terms&condition/Expandatrack_Terms_conditions.pdf'));
                    foreach($exterattachment as $value){
                     $adminmessage->attach(Swift_Attachment::fromPath('uploads/extraattachment/' . $value));
                    }
                   $adminmessage->setBody($request->get('msg'),'text/html');
            $this->get('mailer')->send($adminmessage);
        } else {
            //echo $adminEmail1;die; 
            $adminmessage = \Swift_Message::newInstance() ; 
                    $adminmessage->setSubject($subject);
                    $adminmessage->setFrom(array($adminEmail1=>$adminEmail1));
                    $adminmessage->setReplyTo(array($adminEmail1));
                    $adminmessage->setTo($adminEmail1);
                    $adminmessage->attach(Swift_Attachment::fromPath('uploads/templatepdf/' . $admin_pdf));
                    $adminmessage->attach(Swift_Attachment::fromPath('uploads/terms&condition/Expandatrack_Terms_conditions.pdf'));
                    
                    $adminmessage->setBody($request->get('msg'),'text/html');
            $this->get('mailer')->send($adminmessage);
        }
         $this->get('session')
                ->getFlashBag()
                ->add('success', 'Mail Sent Successfully');
        return $this->redirect($this->generateUrl('order'));
    }
      
}
