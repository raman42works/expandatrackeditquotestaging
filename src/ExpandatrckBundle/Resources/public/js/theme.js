jQuery(document).ready(function ($) {



    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profilepreview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    
//form required attribute
 var is_safari = navigator.userAgent.indexOf("Safari") > -1;
 if(is_safari){
var forms = document.getElementsByTagName('form');
for (var i = 0; i < forms.length; i++) {
    forms[i].noValidate = true;

    forms[i].addEventListener('submit', function(event) {
        //Prevent submission if checkValidity on the form returns false.
        if (!event.target.checkValidity()) {
            event.preventDefault();
            //Implement you own means of displaying error messages to the user here.
            alert('Please fill required fields');
        }
    }, false);
}
 }

//Min height for login
    var headerHeight = $('header').outerHeight();
    $('.contentArea').css('margin-top', headerHeight);
    var height = window.innerHeight;
    var loginHeight = height - 132;
    $('.login').css('min-height', loginHeight);

//Min height for inner pages

    var headerHeight = $('header').height();
    var topBarHeight = $('.serviceTopBar').height();
    var footerHeight = $('#footer').height();
    var innerPagesHeight = headerHeight + footerHeight + 42;
    innerPagesHeight = height - innerPagesHeight;
    $('.contentArea').css('min-height', innerPagesHeight);
    $("#profileInput").change(function () {

        readURL(this);
    });

    $("#fixedcost").click(function () {
        $('#showfixedcost').show();
        $("#showfixedcost").slideDown();
    });
    $("#variablecost").click(function () {
        $('#showfixedcost').hide();

    });
    $('#services').change(function () {
        window.location = $(this).val();
    });
    $(document).on('change', '#serviceparameterlist', function () {

        window.location = $(this).val();
    });
    $('#paraservices').change(function () {

        $.ajax({
            type: "GET",
            url: base_url + 'get/service/parameter',
            data: {'id': $(this).val()},
            success: function (result) {
                console.log(result);
                $('#serviceparameterlist').html(result);
            }
        });
    });
    //$(".datepicker").datepicker({maxDate: 0});

    $(".quote_date").datepicker({
        startDate: new Date(),
        autoclose: true,
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('.expires_date').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('.expires_date').datepicker('setStartDate', null);
    });
    ;
    $(".expires_date").datepicker({
        startDate: new Date(),
        autoclose: true,
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('.quote_date').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('.quote_date').datepicker('setEndDate', null);
    });
    ;



    $('#adduserform').ajaxForm({
        beforeSubmit: function () {
            $("#saveuser").attr('value', 'please wait...');    //Call the reset before the ajax call starts
        },
        success: function (response) {
            var res = response.split(",");
            console.log(res);
            if (res[0] == '1') {
                $("#saveuser").attr('value', 'User Saved');
                toastr.success(res[1]);
                location.reload();
            } else {
                $("#saveuser").attr('value', 'Save Changes');
                toastr.error(res[1]);
            }
        }
    });

//    $(".datepicker").datepicker();

    //Order Create
    $('#orderform').ajaxForm({
        beforeSubmit: function () {
            $("#saveorder").attr('value', 'please wait while adding order.');    //Call the reset before the ajax call starts
        },
        success: function (response) {
            $("#saveorder").attr('value', 'Order Saved');
            $("#orderid").val(response);
            $("#addservice").attr("href", base_url + 'order/' + response + '/add/service/');
            url = base_url + 'order/' + response + '/add/service/';
            $("#saveorder").attr("disabled", true);
            $("#addservice").attr("disabled", false);
            toastr.info('Order Create Successfully.Now you can add the service to this order.');
            setTimeout(function () {
                window.location = url;
            }, 1000);
        }
    });

    $('#add-order-service').ajaxForm(function (response) {
        $("#response").show();
        $("#response").html(response);
        window.setTimeout(function () {
            location.reload()
        }, 3000);
    });

    //password slide down for user profile
    $("#change_user_password").click(function () {

        $("#change_password_text").fadeToggle('slow');
    });
    
    // Add more files 
    var customer_count=0;
    
    $("#add_customer").on('click',function(){
    customer_count++;
    var html='<div class="customer_row" style="margin-top:10px;" id="customer_count-'+customer_count+'">';
    html += '<div class="col-md-8" >';    
    html += '<input type="file" name="user_image[]">';    
      html += '</div>';  
      html += '<div class="col-md-4" >';    
    html += '<a href="javascript:;" onClick="customer_remove('+customer_count+')"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>';
     html += '</div>'; 
        html += '</div>';
    
    $('#member_row_inner').after(html);
        
    //$('#customer_members').append(html);
});




});

if ($("#ptxtConfirmPassword").length) {
    var userpassword = document.getElementById("ptxtNewPassword");
    var user_confirm_password = document.getElementById("ptxtConfirmPassword");
    function validateUserPassword() {

        if (userpassword.value != user_confirm_password.value) {
            user_confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            user_confirm_password.setCustomValidity('');
        }
    }
    userpassword.onchange = validateUserPassword;
    user_confirm_password.onkeyup = validateUserPassword;
}
if ($("#ctxtConfirmPassword").length) {
    var password = document.getElementById("ctxtNewPassword");
    var confirm_password = document.getElementById("ctxtConfirmPassword");
    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
}
function addService(id) {

    $.ajax({
        type: "GET",
        url: base_url + 'add/service/to/order/',
        data: {'id': id},
        success: function (result) {
            console.log('#serviceparameter-' + id);
            $('#serviceparameter-' + id).html(result);
        }
    });
}
function updateOrderService(id) {

    $.ajax({
        type: "GET",
        url: base_url + 'update/service/to/order/',
        data: {'id': id},
        success: function (result) {
            console.log('#serviceparameter-' + id);
            $('#serviceparameter-' + id).html(result);
        }
    });
}
$('.changePassToggle').click(function () {
    $('.chnagePasswordContent').toggleClass('showPass');


});
$('#alter_box').click(function () {
    $('.alterText').toggleClass('showPass');


});
function changePassword(id) {

    $("#change_password_container-" + id).slideToggle('slow');
}
function imagePreview(value, id) {
    if (value.files && value.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {

            $('.preivew-' + id).attr('src', e.target.result);
        }

        reader.readAsDataURL(value.files[0]);
    }
}
function imageAddPreview(value) {
    if (value.files && value.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {

            $('.preivew').attr('src', e.target.result);
        }

        reader.readAsDataURL(value.files[0]);
    }
}

function chnageDesc(id) {

    $("#desc-" + id).hide();
    $("#description-text-" + id).show();
    $("#description-text-" + id).focus();
    $("#description-text-" + id).val();
}
function checkDesc(obj, id) {

    $("#desc-" + id).html(obj.value);
    $("#desc-" + id).show();
    $("#description-text-" + id).hide();
    $.ajax({
        type: "GET",
        url: base_url + 'add/desc',
        data: {'id': id, description: obj.value},
        success: function (result) {
            console.log(result);

        }
    });

}
// Change Other text
function chnageOtherText() {

    $("#othertext").hide();
    $("#edit-other-text").show();
    $("#edit-other-text").focus();
    $("#edit-other-text").val();
}
function checkOtherTextValue(obj, id) {

    $("#othertext").html(obj.value);
    $("#othertext").show();
    $("#edit-other-text").hide();
    $.ajax({
        type: "GET",
        url: base_url + 'add/other/text',
        data: {'id': id, othertext: obj.value},
        success: function (result) {
            console.log(result);

        }
    });

}
// change span to text ediatable
function chnageValue(id) {

    $("#qtyvalue-" + id).hide();
    $("#qty-" + id).show();
    $("#qty-" + id).focus();
    $("#qty-" + id).val();
}

function checkValue(obj, cost, id) {

    $("#qtyvalue-" + id).html(obj.value);
    $("#qtyvalue-" + id).show();
    $("#qty-" + id).hide();


    //ankits code
    var quantity = obj.value;
    var perunit = $("#instcost-" + id).val();

    var final = parseInt(quantity) * parseInt(perunit);


    console.log($("#totalinstcost-" + id));
    $("input#totalinstcost-" + id).attr("value", final);

    var ankit_total = 0;
    $(".ankit_field").each(function () {
        ankit_total += parseInt($(this).val());
    });

    $.ajax({
        type: "GET",
        url: base_url + 'add/qty/',
        data: {'id': id, qty: obj.value},
        success: function (result) {
            console.log(result);
            $('#serviceparameter').html(result);
        }
    });

    var res = parseInt(obj.value) * parseInt(cost);

    $("#ammount-" + id).html('$' + res);
    var sum = 0;
    $('.total-ammount').each(function () {
        var value = $(this).html().split("$");

        sum += +parseInt(value[1]);
        console.log("value" + parseInt(value[1]));
    });
    $("#installationCost").html("$" + ankit_total);

    var installationCost = $("#installationCost").html().split("$");
    var service_cost = $("#change-service-cost").html().split("$");
    var travel_cost = $("#travel-cost").html().split("$");
    var other = $("#other").html().split("$");
    var grand_total = parseInt(sum) + parseInt(installationCost[1]) + parseInt(service_cost[1]) + parseInt(travel_cost[1]) + +parseInt(other[1]);

    //var total_install_cost =  parseInt(installationCost[1]) * parseInt(obj.value);

    $("#sub-total").html("$" + sum);

    $("#grandTotal").html("$" + grand_total);
}
function checkServiceCostValue(obj, cost, id) {

    $("#change-cost").html(obj.value);
    $("#change-service-cost").show();
    $("#change-cost").hide();
    
    if($.isNumeric(obj.value)) {
        //return true;
    } else {
        alert('Please enter a valid number!');
         $("#change-cost").show();
         $("#change-service-cost").hide();
         $("#change-cost").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: base_url + 'add/service/cost',
        data: {'id': id, servicecost: obj.value},
        success: function (result) {
            console.log(result);

        }
    });
    $("#change-service-cost").html('$' + obj.value);
    var sum = 0;

    var installationCost = $("#installationCost").html().split("$");
    var travelcost = $("#travel-cost").html().split("$");
    var other = $("#other").html().split("$");
    var sub_total = $("#sub-total").html().split("$");
    sum = parseInt(sub_total[1]) + parseInt(installationCost[1]) + parseInt(obj.value) + parseInt(travelcost[1]) + parseInt(other[1]);
    console.log("installationCost" + installationCost);
    console.log("travelcost" + travelcost);
    console.log("other" + other);
    console.log("subtotal" + sub_total);
    console.log("sum" + sum);
    $("#grandTotal").html("$" + sum);
}
function checkTravelCostValue(obj, cost, id) {

    $("#travel-cost").html(obj.value);
    $("#travel-cost").show();
    $("#travel-cost-text").hide();
    
    if($.isNumeric(obj.value)) {
        //return true;
    } else {
        alert('Please enter a valid number!');
         $("#travel-cost-text").show();
         $("#travel-cost").hide();
         $("#travel-cost-text").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: base_url + 'add/travel/cost',
        data: {'id': id, servicecost: obj.value},
        success: function (result) {
            console.log(result);

        }
    });
    $("#travel-cost").html('$' + obj.value);
    var sum = 0;

    var installationCost = $("#installationCost").html().split("$");
    var service_cost = $("#change-service-cost").html().split("$");
    var other = $("#other").html().split("$");
    var sub_total = $("#sub-total").html().split("$");
    sum = parseInt(sub_total[1]) + parseInt(installationCost[1]) + parseInt(obj.value) + parseInt(service_cost[1]) + parseInt(other[1]);

    console.log("sum" + sum);
    $("#grandTotal").html("$" + sum);
}
function checkOtherCostValue(obj, cost, id) {

    $("#other").html(obj.value);
    $("#other").show();
    $("#other-cost").hide();
    
    
if($.isNumeric(obj.value)) {
        //return true;
    } else {
        alert('Please enter a valid number!');
         $("#other-cost").show();
         $("#other").hide();
         $("#other-cost").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: base_url + 'add/other/cost',
        data: {'id': id, othercost: obj.value},
        success: function (result) {
            console.log(result);

        }
    });
    $("#other").html('$' + obj.value);
    var sum = 0;

    var installationCost = $("#installationCost").html().split("$");
    var service_cost = $("#change-service-cost").html().split("$");
    var travel_cost = $("#travel-cost").html().split("$");
    var sub_total = $("#sub-total").html().split("$");
    sum = parseInt(sub_total[1]) + parseInt(installationCost[1]) + parseInt(obj.value) + parseInt(service_cost[1]) + parseInt(travel_cost[1]);

    console.log("sum" + sum);
    $("#grandTotal").html("$" + sum);
}
function chnageServiceCost() {

    $("#change-service-cost").hide();
    $("#change-cost").show();
    $("#change-cost").focus();
    $("#change-cost").val();
}
function chnageTravelCost() {

    $("#travel-cost").hide();
    $("#travel-cost-text").show();
    $("#travel-cost-text").focus();
    $("#travel-cost-text").val();
}
function chnageOtherCost() {

    $("#other").hide();
    $("#other-cost").show();
    $("#other-cost").focus();
    $("#other-cost").val();
}
function fixedCost(id) {
    $("#showfixedcost-" + id).show();

}
function variableCost(id) {
    $("#showfixedcost-" + id).hide();
    $("#costvalue").removeAttr("value");

}
function changeFixedCost(id) {
    $("#cost-" + id).show();

}
function changevariableCost(id) {

}
function changeFixedCost(id) {
    $("#cost-" + id).show();

}
function changevariableCost(id) {

    $("#cost-" + id).hide();

}

function includeInstallationCost(id) {

    if ($("#include_installation_cost").is(':checked')) {
        var include_cost = 1;
    } else {

        var include_cost = 0;
    }
    $.ajax({
        type: "GET",
        url: base_url + 'order/include/installation/cost',
        data: {'id': id, 'include': include_cost},
        success: function (result) {
            if (include_cost) {
                var total = $("#grandTotal").html().split("$");
                var cost = $('#includecost').val();
                var grandtotal = parseFloat(total[1]) - parseFloat(cost);
                $('#installationCost').html("$0");
                $("#grandTotal").html('$' + grandtotal);
                $("#grand-total-invoice").html();
                $("#grand-total-invoice").html('$' + ((parseFloat(grandtotal) * parseFloat(0.15)).toFixed(2) ));
                
            } else {
                var total = $("#grandTotal").html().split("$");
                var cost = $('#includecost').val();
                var grandtotal = parseFloat(total[1]) + parseFloat(cost);
                $('#installationCost').html('$' + cost);
                $("#grandTotal").html('$' + grandtotal);
                $("#grand-total-invoice").html();
                $("#grand-total-invoice").html('$' + ((parseFloat(grandtotal) * parseFloat(0.15)).toFixed(2) ) );
            }
            console.log(result);
            $('#serviceparameterlist').html(result);
        }
    });
}


// Paramteter cost Value change
function chnageParameterCost(id) {

    $("#parameter-cost-" + id).hide();
    $("#parameter-cost-text-" + id).show();
    $("#parameter-cost-text-" + id).focus();
    $("#parameter-cost-text-" + id).val();
}

function checkParameterValue(obj, id) {

    $("#parameter-cost-text-" + id).html(obj.value);
    $("#parameter-cost-text-" + id).show();
    $("#parameter-cost-text-" + id).hide();
    $.ajax({
        type: "GET",
        url: base_url + '/edit/parameter/inner/cost/',
        data: {'id': id, cost: obj.value},
        success: function (result) {
            console.log(result);

        }
    });

}

// Manual Cost

function chnageManualCost() {

    $("#manual-cost").hide();
    $("#change-manual-cost").show();
    $("#change-manual-cost").focus();
    $("#change-manual-cost").val();
}
function checkManualCostValue(obj, cost, id) {

    $("#manual-cost").html(obj.value);
    $("#change-manual-cost").hide();
    $("#manual-cost").show();
    
    
    if($.isNumeric(obj.value)) {
        //return true;
    } else {
        alert('Please enter a valid number!');
         $("#change-manual-cost").show();
         $("#manual-cost").hide();
         $("#change-manual-cost").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: base_url + 'add/manual/cost',
        data: {'id': id, cost: obj.value},
        success: function (result) {
            console.log(result);

        }
    });
        if(obj.value == null || obj.value == ''){
            $("#manual-cost").html('$' + 0);
            obj.value = 0;
        } else {
            $("#manual-cost").html('$' + obj.value);
        }
    var sum = 0;

    var installationCost = $("#installationCost").html().split("$");
    var serviceCost = $("#change-service-cost").html().split("$");
    var travelcost = $("#travel-cost").html().split("$");
    var other = $("#other").html().split("$");
    var sub_total = $("#sub-total").html().split("$");
    sum = parseInt(sub_total[1]) + parseInt(serviceCost[1]) + parseInt(installationCost[1]) + parseInt(obj.value) + parseInt(travelcost[1]) + parseInt(other[1]);
    console.log("installationCost" + installationCost);
    console.log("travelcost" + travelcost);
    console.log("other" + other);
    console.log("subtotal" + sub_total);
    console.log("sum" + sum);
    $("#grandTotal").html("$" + sum);
}

function includeManualCost(id) {

    if ($("#include_installation_manual_cost").is(':checked')) {
        var include_cost = 1;
    } else {

        var include_cost = 0;
    }
    $.ajax({
        type: "GET",
        url: base_url + 'order/include/manual/cost',
        data: {'id': id, 'include': include_cost},
        success: function (result) {
            if (include_cost) {
                
                var total = $("#grandTotal").html().split("$");
                var cost = $('#manualincludecost').val();
                var grandtotal = parseFloat(total[1]) - parseFloat(cost);
                $('#manual-cost').html("$0");
                $("#grandTotal").html('$' + grandtotal);
                $("#grand-total-invoice").html();
                $("#grand-total-invoice").html('$' + ((parseFloat(grandtotal) * parseFloat(0.15)).toFixed(2) ));
                
            } else {
                var total = $("#grandTotal").html().split("$");
                var cost = $('#manualincludecost').val();
                var grandtotal = parseFloat(total[1]) + parseFloat(cost);
                $('#manual-cost').html('$' + cost);
                $("#grandTotal").html('$' + grandtotal);
                $("#grand-total-invoice").html();
                $("#grand-total-invoice").html('$' + ((parseFloat(grandtotal) * parseFloat(0.15)).toFixed(2) ) );
            }
            console.log(result);
            $('#serviceparameterlist').html(result);
        }
    });
}

function send_quotation_email(id){
    checked = $("input[type=checkbox]:checked").length;

    if(!checked) {
      alert("You can't include both auto and manual installation cost together.");
      return false;
    }else if(checked == 2){
       var r = confirm("Both Installation cost is 0 , Are you sure you want to continue?? ");
        if (r == true) {
         txt = location.href=base_url+"send/quotation/email/"+id;
        } 
    }
    
    else{
        
        location.href=base_url+"send/quotation/email/"+id;
    }
}
function send_invoice_email(id){
    checked = $("input[type=checkbox]:checked").length;
   
    if(!checked) {
      alert("You can't include both auto and manual installation cost together.");
      return false;
    } else if(checked == 2){
        var r = confirm("Both Installation cost is 0 , Are you sure you want to continue?? ");
        if (r == true) {
         txt = location.href=base_url+"send/invoice/email/"+id;
        }
        
    } else {
        
        location.href=base_url+"send/invoice/email/"+id;
  
    }
    
    
}
function customer_remove(count){    
   
  $('#customer_count-'+count).remove();
}

function checkInstallationcost(url){
    
     checked = $("input[type=checkbox]:checked").length;
     if(!checked) {
      alert("You can't include both auto and manual installation cost together.");
      return false;
    }else if(checked == 2){
        var a = confirm("Both Installation cost is 0 , Are you sure you want to continue?? ");
        if(a==true){
            var link = document.createElement('a');
            link.href = url;
            link.download =  url;
            document.body.appendChild(link);
            link.click();
        }
     }else if(checked == 1 || checked == 0){
            var link = document.createElement('a');
            link.href = url;
            link.download =  url;
            document.body.appendChild(link);
            link.click();
     }
}
function checkInstallationcostOne(){
     var editor = CKEDITOR.instances['msg'];
     if (editor) { editor.destroy(true); }

     checked = $("input[type=checkbox]:checked").length;
     if(!checked) {
      alert("You can't include both auto and manual installation cost together.");
      return false;
    }else if(checked == 2){
        var a =  confirm("Both Installation cost is 0 , Are you sure you want to continue?? ");
         if(a==true){
         CKEDITOR.replace('msg');    
        $('#manual-email').modal('show');
         }else{
             $('#manual-email').modal('hide');
             
         }
        
     }else if(checked == 1 || checked == 0){
         CKEDITOR.replace('msg');
         $('#manual-email').modal('show');
     }
}
//get parameter dropdown options via ajax-PRODUCT EDIT DROPDOWN
function GetParameterOptions(parameter_id){
    console.log(parameter_id); 
} 