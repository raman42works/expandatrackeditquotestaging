<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ExpandatrckBundle\Twig;
use Twig_Extension;
use Twig_Filter_Method;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\User\UserInterface;

class AppExtension extends Twig_Extension {
    
    protected $em;
    protected $container;
    private $_router;

   public function __construct(EntityManager $em,Container $container,Router $router){
      $this->em = $em;
      $this->container = $container;
      $this->_router = $router;
   }
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('getOrderService', array($this, 'getOrderService')),
            new \Twig_SimpleFilter('getServiceName', array($this, 'getServiceName')),
            new \Twig_SimpleFilter('getInstallationcost', array($this, 'getInstallationcost')),
            new \Twig_SimpleFilter('getVariationcost', array($this, 'getVariationcost')),
            new \Twig_SimpleFilter('getParameters', array($this, 'getParameters')),
            new \Twig_SimpleFilter('getAlterText', array($this, 'getAlterText')),
            new \Twig_SimpleFilter('getTotalService', array($this, 'getTotalService')),
            new \Twig_SimpleFilter('getTotalCost', array($this, 'getTotalCost')),
            new \Twig_SimpleFilter('getServiceCost', array($this, 'getServiceCost')),
            new \Twig_SimpleFilter('getCustomerParameters', array($this, 'getCustomerParameters')),
            new \Twig_SimpleFilter('getParametersCost', array($this, 'getParametersCost')),
            new \Twig_SimpleFilter('getInvoiceParametersCost', array($this, 'getInvoiceParametersCost')),
            new \Twig_SimpleFilter('getTaxStatus', array($this, 'getTaxStatus')),
            new \Twig_SimpleFilter('isSubscribed', array($this, 'isSubscribed')),
             new \Twig_SimpleFilter('asset_exists', array($this, 'asset_exists')),
          
        );
    }

    public function asset_exists($name="",$service_id=""){
        $path = "uploads/variation/".$service_id.'/'.$name;
          if(file_exists($path)){
             return 1;
          }else{
              return 0;
          }
    }
    public function getOrderService($id) {
        
        $sql = "SELECT *  FROM service_order WHERE order_id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
                 return $stmt->fetchAll();
    }
    public function getTaxStatus($status){

      if($status==1){
        return "Accepted";
      }elseif($status==2){
        return "Rejected";
      }elseif($status==3){
        return "Pending";
      }elseif($status==4){
        return "Completed";
      }
    }
    public function isSubscribed($user_id=0){

       $sql = "SELECT count(*)  FROM  order WHERE subscription=1 AND user_id = ".$user_id;
       $stmt = $this->em->getConnection()->prepare($sql);
       $stmt->execute();
       $data  = $stmt->fetchAll();
    }
    public function getServiceName($id) {
        
        $sql = "SELECT *  FROM service WHERE id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
                 return $stmt->fetch()['service_name'];
    }
    public function getVariationcost($id) {
        
        $sql = "SELECT *  FROM variation WHERE id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
                 return $id;
    }
    public function getInstallationcost($id) {
        
        $sql = "SELECT *  FROM installationcost WHERE id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
                 return $stmt->fetch()['cost'];
    }
    public function getParameters($json) {
       
      $array = unserialize($json);
      $str = 'Specifications:';
      $para_arr = array();
      foreach($array as $key=>$val){
          if($key == 'parameter'){
            
              $parameters = unserialize($val);
              if(!empty($parameters)){
                foreach($parameters as $parakey => $para){
                    $para_arr = explode('~',$para);
                    $str .= $para_arr[0].','; 
                }
              }
          } else {
              if($val != 0){
                $str .= $key.':'.$val.' ';
              }
          }
      }
   
     return rtrim($str,",");
    }
    public function getCustomerParameters($json,$user_id=0) {
      
      $array = unserialize($json);
      $str = 'Specifications:';
      $para_arr = array();

      //echo $user_id;die;
      

      foreach($array as $key=>$val){
          if($key == 'parameter'){ 
            
              $parameters = unserialize($val);
              //$parameters[]=$array['Width']; 
              //$parameters[]=$array['Height'];  
              if($user_id==1){
                 $inserted = array('width: '.@$array['Width'].' height: '.@$array['Height']); // Not necessarily an array
                 if($parameters != ""){
                    array_splice( $parameters, 0, 0, $inserted ); // splice in at position 0
                 }
                 
              }
             


              if(!empty($parameters)){
                foreach($parameters as $parakey => $para){
                    $para_arr = explode('~',$para);
                    $str .= $para_arr[0].','; 
                }
              }
          }
      }
      //echo '<pre>';
      //print_r($str);
      //echo '</pre>';die;
     return rtrim($str,",");
    }
   
    
    public function getName() {
        return 'app_extension';
    }
    public function getAlterText($id) {
        
        $sql = "SELECT *  FROM variation WHERE id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
        $aler_text =  $stmt->fetch()['alter_text'];
        return !empty($aler_text) ? rtrim($aler_text,",") : 'N/A';
    }
    public function getTotalService($id,$order_id) {
      
      
        $sql = "SELECT *  FROM service_order WHERE service_id = ".$id." AND order_id=".$order_id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
        $result = $stmt->fetchAll();
        $total =0;
       foreach($result as $val ){
           $total += $val['qty'];
       }
       return $total;
    }
    public function getTotalCost($id) {
        
        $sql = "SELECT * FROM service_order WHERE service_id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
        $installationid =  $stmt->fetchAll();
        $total = 0;
        foreach($installationid as $val){
           $id = $val['installationcost_id'];
           $sql = "SELECT * FROM installationcost WHERE id = ".$id;
                 $stmt = $this->em->getConnection()->prepare($sql);
                 $stmt->execute();
            $cost =  $stmt->fetch()['cost'];
            $total = $total + $cost ;
        }
        return $total;
    }
    
    function getServiceCost($id,$order_id){
        
            
                $total=0;

                $dql = "SELECT * FROM service_order WHERE service_id = ".$id. " and order_id = $order_id";
                $dql = $this->em->getConnection()->prepare($dql);
                $dql->execute();
                $record =   $dql->fetchAll();
                
           
                
               
                if(!empty($record)){
                    
                    foreach($record as $val){
                        
                         if($this->check_service($val['service_id'])=='variablecost'){
                                $dql1 = "SELECT *  FROM variation WHERE id = ".$val['variation_id'];
                                $dql1 = $this->em->getConnection()->prepare($dql1);
                                $dql1->execute();
                                $record1 =   $dql1->fetch()['cost'] ;
                                $total += $record1 * $val['qty'];
                         }else{
                             
                                $dql2 = "SELECT *  FROM service WHERE id = ".$id;
                                $dql2 = $this->em->getConnection()->prepare($dql2);
                                $dql2->execute();
                                $record2 =   $dql2->fetch()['cost'];
                                $total += $record2 * $val['qty'];
                         }
                    }
                }
                
         
            return $total;
    }
    
    public function check_service($service_id){
        $sql = "SELECT * FROM service WHERE   id = $service_id";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetch()['cost_type']; 
    }
    
    public function getParametersCost($json){
           $array = unserialize($json);
           $total_cost = 0;
           if(!empty($array)){
               foreach($array as  $paracost){
                    foreach($paracost as $cost){
                        $total_cost += $cost;
                    }
               }
               return $total_cost;
           } else {
               return 0;
           }
    }
    public function getInvoiceParametersCost($service_id=null,$order_id=null){
        
           $dql = "SELECT * FROM service_order WHERE service_id = ".$service_id. " and order_id = $order_id";
           $dql = $this->em->getConnection()->prepare($dql);
           $dql->execute();
           $record =   $dql->fetchAll();
           
           $total_cost = 0;
           foreach($record as $val){
            $array = unserialize($val['parameter']);
            if(!empty($array)){
                foreach($array as  $paracost){
                     foreach($paracost as $cost){
                         $total_cost += $cost * $val['qty'];
                     }
                }
               
            }
          }
          return $total_cost;
    }
    
}
