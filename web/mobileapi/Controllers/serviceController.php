<?php

class serviceController extends Controller {

    /**
     * Get  Services 
     * @param 
     * @return All Services Data
     */
    function getServices() {
        require(ROOT . 'Models/Services.php');
        //if ($_POST['token'] == 'ExpandABestTrack951') {
            if ('ExpandABestTrack951' == 'ExpandABestTrack951') {
            $d[] = '';
            $services = new Services();
            $timestamp = $_POST['timestamp'];
            // $timestamp = '2018-11-01 06:54:23';
            $variation = '';
            $allData = array();
            $response = array();

            if ($timestamp) {
                $d['Services'] = $services->showAllServiceswithTime($timestamp);
                $d['parameter'] = $services->showAllServicesVariationwithTime($timestamp);
                $d['instalation'] = $services->showAllinstalationsCostwithTime($timestamp);
                $d['variations'] = $services->showAllVariationswithTime($timestamp);
            } else {
                $d['Services'] = $services->showAllServices();
                $d['parameter'] = $services->showAllServicesVariation();
                $d['instalation'] = $services->showAllinstalationsCost();
                $d['variations'] = $services->showAllVariations();
            }

            $servicesData = $d['Services'];
            $parameter = $d['parameter'];
            $instalation = $d['instalation'];
            $variations = $d['variations'];
            if ($servicesData || $parameter || $instalation || $variations) {
                $count = -1;
                foreach ($servicesData as $serv) {
                    $count++;
                    $servicesIds = $serv['id'];
                    $serviceFullData[$count]['id'] = $servicesIds;
                    $serviceFullData[$count]['name'] = $serv['service_name'];
                    $serviceFullData[$count]['cost_type'] = $serv['cost_type'];
                    $serviceFullData[$count]['cost'] = $serv['cost'];
                }

                $allData['services'] = $servicesData;
                $allData['installation'] = $instalation;
                $allData['variations'] = $variations;
                $allData['parameter'] = $parameter;

                $response = array(
                    'status' => "true",
                    'message' => "successful",
                    'date' => date('Y-m-d H:i:s'),
                    'data' => $allData,
                );
            } else {
                $response = array(
                    'status' => "false",
                    'message' => "No Services found"
                );
            }
        } else {
            $response = array(
                'status' => "false",
                'message' => "Token Required"
            );
        }
        echo json_encode($response); // send response  
    }

    /**
     * Get Service Variation
     * @param ServiceId
     * @return Service Variation
     */
    function getServicesVariation() {
        require(ROOT . 'Models/Services.php');
        $d = '';
        $services = new Services();
        $response = array();
        $serviceID = 18;
        //$serviceID = $_POST['serviceid'];
        $servicesData = $services->showServicesVariation($serviceID);

        if ($servicesData) {
            $response = array(
                'status' => "true",
                'services' => $servicesData,
            );
        } else {
            $response = array(
                'status' => "false",
                'message' => "Not service variation found"
            );
        }

        echo json_encode($response); // send response  
    }

    /**
     * Get Variation Options
     * @param VariationID
     * @return variation options
     */
    function getVariationOptions() {
        require(ROOT . 'Models/Services.php');
        $d = '';
        $services = new Services();
        $response = array();
        //$serviceID = $_POST['serviceid'];
        //$variationID = $_POST['variationid'];
        $serviceID = 53;
        $variationID = 53;
        $servicesData = $services->showVariationOption($variationID);

        if ($servicesData) {
            $response = array(
                'status' => "true",
                'services' => $servicesData,
            );
        } else {
            $response = array(
                'status' => "false",
                'message' => "No variation options found"
            );
        }

        echo json_encode($response); // send response  
    }

    /**
     * Get Variation Price
     * @param ServiceId
     * @param  startheight
     * @param  endheight
     * @param  startwidth
     * @param  endwidth
     * @return Cost of variation  
     */
    function getVariationPrice() {
        require(ROOT . 'Models/Services.php');
        $d = '';
        $services = new Services();
        $response = array();
        $serviceID = $_POST['serviceid'];
        $height = $_POST['height'];
        $width = $_POST['width'];
//        $serviceID = 18;
//        $height = 1600;
//        $width = 1500;
        $servicesData = $services->showVariationPrice($serviceID, $height, $width);

        if ($servicesData) {
            $response = array(
                'status' => "true",
                'services' => $servicesData,
            );
        } else {
            $response = array(
                'status' => "false",
                'message' => "No Variation Cost exist"
            );
        }
        echo json_encode($response); // send response  
    }

    /**
     * Get Variation Price
     * @param ServiceId   
     * @return Service Order ID  
     */
    function addServiceOrder() {

        require(ROOT . 'Models/Services.php');
        $d = '';
        $services = new Services();
        $response = array();
        foreach ($serviceIDss as $servicess) {
            $serviceID = $servicess['service_id'];
            $varation = $servicess['variation'];
            $serlizedVariation = serialize($varation);

            $metaUnserial = array('width' => $width, 'height' => $height, 'parameter' => $varation);
            $meta = serialize($metaUnserial);

            $orderid = 39;

            $serviceID = 18;
            $height = 1600;
            $width = 1500;

            $variation = $services->showVariationID($serviceID, $height, $width);
            $variationID = $variation[0]['id'];

            $instaltion = $services->showinstalationID($serviceID, $width);
            $instaltionID = $instaltion[0]['id'];

            $instaltionC = $services->showinstalationCost($instaltionID);
            $instaltionCost = $instaltionC[0]['cost'];

            $varationData = $services->showVariationPrice($serviceID, $height, $width);
            $varationCost = $varationData[0]['cost'];

            $varOption = array(1244 => 1310, 1188 => 1312, 1008 => 1316);
            foreach ($varOption as $varoptions) {
                $pmcost[] = $services->showparameterCost($varoptions);
            }
            $ttcost = 0;

            $servicemeta = '';


            foreach ($pmcost as $pmcostss) {
                $ttcost = $ttcost + $pmcostss;
            }
            $tottalServiceCost = $instaltionCost + $ttcost + $instaltionCost + $varationCost;

            echo $tottalServiceCost;

            $createdDate = date("Y-m-d h:i:sa");
            $customertext = 'hello';

            $qty = 1;
            $servicesData = $services->addserviceOrder($orderid, $serviceID, $variationID, $instaltionID, $meta, $serlizedVariation, $createdDate, $qty, $customertext);

            if ($servicesData) {
                $response[] = array(
                    'status' => "true",
                    'servicesOrderID' => $servicesData,
                );
            } else {
                $response[] = array(
                    'status' => "false",
                    'message' => "No Variation Cost exist"
                );
            }
        }
        echo json_encode($response); // send response  
    }

    

}

?>