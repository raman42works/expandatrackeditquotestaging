<?php

class orderController extends Controller {

    function index() {
        require(ROOT . 'Models/Order.php');
        $tasks = new Order();
        $d['tasks'] = $tasks->showAllServices();
        $this->set($d);
        $this->render("index");
    }

    /**
     * Get Create Order
     * @param All Data of Started order form
     * @return Order ID 
     */
    function createOrder() {
        require(ROOT . 'Models/Order.php');
        $d[] = '';
        $orderidd = array();
        $ssidd = array();
        $order = new Order();
        //$Fulldata = '[{"order":{"user_id":"","name":"Shsjs","email":"hahah@jeue.com","phone":"494944","address":"Vavabw","quote_date":"2019-02-08 05:30:00","expires":"2019-02-08 05:30:00","quotation":"","quoteno":"","travel_cost":"","is_include":"","manual_cost":"","is_manual_include":"","subscription":"1","taxstatus":"3","service_cost":"0","other":"0","other_text":"","description":"","signature":""},"services":[{"service_id":"23","variation_id":"92","installationcost_id":"34","meta":{"width":"1200","height":"1500","parameter":["Ashwood Halo~0","Inverted Pleat(3600-4599)~80","Triple Pleat(3600-4599)~80","~","~","~","~","~"]},"created_at":"2019-02-08 06:26:21 pm","updated_at":"2019-02-08 06:26:21 pm","travel_cost":"","description":"","qty":"1","service_cost":"","other":"","customer_text":""}]}]';
        $Fulldata = $_POST['data'];
        //if ($token == 'ExpandABestTrack951') {
        if ('ExpandABestTrack951' == 'ExpandABestTrack951') {
            if ($Fulldata) {
                $data = json_decode($Fulldata);
                foreach ($data as $orderInfo) {
                    $Orderinfo = $orderInfo->order;
                    $user_id = $Orderinfo->user_id;
                    $name = $Orderinfo->name;
                    $email = $Orderinfo->email;
                    $phone = $Orderinfo->phone;
                    $address = $Orderinfo->address;
                    $quote_date = $Orderinfo->quote_date;
                    $expires = $Orderinfo->expires;
                    $quotation = $Orderinfo->quotation;
                    $quoteno = $Orderinfo->quoteno;
                    $travel_cost = $Orderinfo->travel_cost;
                    $is_includes = $Orderinfo->is_include;
                    if ($is_includes) {
                        $is_include = $is_includes;
                    } else {
                        $is_include = 0;
                    }
                    $manual_cost = $Orderinfo->manual_cost;
                    $is_manual_include = $Orderinfo->is_manual_include;
                    $subscription = $Orderinfo->subscription;
                    if ($subscriptions) {
                        $subscription = $subscriptions;
                    } else {
                        $subscription = 0;
                    }
                    $taxstatus = $Orderinfo->taxstatus;
                    $service_cost = $Orderinfo->service_cost;
                    $other = $Orderinfo->other;
                    $other_text = $Orderinfo->other_text;
                    $description = $Orderinfo->description;
                    $signature = $Orderinfo->signature;

                    $d['tasks'] = $order->createNewOrder($user_id, $name, $email, $phone, $address, $quote_date, $expires, $quotation, $quoteno, $travel_cost, $is_include, $manual_cost, $is_manual_include, $subscription, $taxstatus, $service_cost, $other, $other_text, $description, $signature);
                    $orderID = $d['tasks'];
                    if ($orderID) {
                        $allservice = $orderInfo->services;
                        foreach ($allservice as $serv) {
                            $service_id = $serv->service_id;
                            $variation_id = $serv->variation_id;
                            $installationcost_id = $serv->installationcost_id;
                            $meta = $serv->meta;
                            $parameter = $serv->parameter;
                            $created_at = $serv->created_at;
                            $updated_at = $serv->updated_at;
                            $travel_cost = $serv->travel_cost;
                            $description = $serv->description;
                            $qty = $serv->qty;
                            $service_cost = $serv->service_cost;
                            $other = $serv->other;
                            $customer_text = $serv->customer_text;

                            $metaData = $serv->meta;

                            //$mainmetaData = json_decode($metaData);
                            //print_r($metaData->width);

                            $wid = $metaData->width;
                            $heigh = $metaData->height;
                            $parameterlist = $metaData->parameter;
                            $metaAll = array('Width' => $wid, 'Height' => $heigh, 'parameter' => serialize($parameterlist));
                            $meta = serialize($metaAll);
                            $parameter_cost = array();
                            if (!empty($parameterlist)) {
                                foreach ($parameterlist as $val) {
                                    $para_arr = explode('~', $val);

                                    $parameter_cost[] = array($para_arr[0] => $para_arr[1]);
                                }
                            }
                            $parameter = serialize($parameter_cost);

                            $d['service'] = $order->addserviceOrder($orderID, $service_id, $variation_id, $installationcost_id, $meta, $parameter, $created_at, $updated_at, $travel_cost, $description, $qty, $service_cost, $other, $customer_text);
                            $ssidd[$orderID] = $d['service'];
                        }
                    }
                }
                if ($orderID) {
                    $response = array(
                        'status' => "true",
                        'message' => "successful",
                    );
                } else {
                    $response = array(
                        'status' => "false",
                        'message' => "No Order Created"
                    );
                }
            } else {
                $response = array(
                    'status' => "false",
                    'message' => "No data found",
                );
            }
        } else {
            $response = array(
                'status' => "false",
                'message' => "No Tokan Found"
            );
        }

        print_r(json_encode($response));
    }

    function uploadSignature() {
        require(ROOT . 'Models/Order.php');
        $d[] = '';
        $order = new Order();
        $token = $_POST['token'];

        if ($token == 'ExpandABestTrack951') {
            //if ('ExpandABestTrack951' == 'ExpandABestTrack951') {
            if ($_FILES["path"]) {
                $target_dir = ROOT . "upload/signature/";
                $ttPath = 'http://198.211.119.12/mobileapi/upload/user/';
                $randnum = 'sign' . rand(1001, 195485556);
                $target_file = $target_dir . $randnum . basename($_FILES["path"]["name"]);
                if (move_uploaded_file($_FILES["path"]["tmp_name"], $target_file)) {
                    $totalpath = $ttPath . $randnum . basename($_FILES["path"]["name"]);

                    $response = array(
                        'status' => "true",
                        'message' => 'successful',
                        'path' => $totalpath
                    );
                } else {
                    $totalpath = ' ';
                    $response = array(
                        'status' => "false",
                        'message' => 'Error in uploading Image',
                        'path' => $totalpath
                    );
                }
            } else {
                $response = array(
                    'status' => "false",
                    'message' => 'Image Required',
                    'path' => ''
                );
            }
        } else {
            $response = array(
                'status' => "false",
                'message' => 'Token Required'
            );
        }
        print_r(json_encode($response));
    }

}

?>