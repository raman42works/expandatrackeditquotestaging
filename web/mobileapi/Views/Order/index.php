<h1>Create Order</h1>
<div class="fullwidth">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <form method='post' action='createOrder'>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label for="title">Name</label>
                        <input type="text" class="form-control" id="title" placeholder="Name" name="username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="description">Email</label>
                        <input type="email" class="form-control" placeholder="Email" name="useremail">
                    </div>
                    <div class="col-sm-6">
                        <label for="description">Phone</label>
                        <input type="text" class="form-control"  placeholder="Phone Number" name="phone">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label for="title">Address</label>
                        <input type="text" class="form-control"  placeholder="Address" name="address">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label for="description">Quota Date</label>
                        <input type="text" class="form-control"  placeholder="YYYY-MM-DD 00:00:00" name="qdate">
                    </div>
                    <div class="col-sm-6">
                        <label for="description">Expire Date</label>
                        <input type="text" class="form-control"  placeholder="YYYY-MM-DD 00:00:00" name="edate">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label for="title">Extra Notes</label>
                        <input type="text" class="form-control"  placeholder="Extra Notes" name="extranote">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label for="title">Tax Status</label><br/>
                        <input type="radio" name="taxstatus" value="1" /> Accepted
                        <input type="radio" name="taxstatus" value="2" /> Reject
                        <input type="radio" name="taxstatus" value="3" /> Pending
                        <input type="radio" name="taxstatus" value="4" /> Completed
                    </div>
                </div>
                <input type="hidden" name="tokan" value="Expand0001" />
                <button type="submit" class="btn btn-primary">Submit</button>               
            </form>
        </div>
    </div>
</div>
<style>
    .form-group .col-sm-6, .form-group .col-sm-12{
        float: left;
        text-align: left;
        margin-bottom:20px;
    }
</style>
<?php
//$kk = 'a:3:{s:5:"Width";s:4:"2350";s:6:"Height";s:4:"2200";s:9:"parameter";s:115:"a:8:{i:0;s:13:"Ashwood Halo~";i:1;s:1:"~";i:2;s:1:"~";i:3;s:1:"~";i:4;s:1:"~";i:5;s:1:"~";i:6;s:1:"~";i:7;s:1:"~";}";}';
//$pp = unserialize($kk);
//$pp1 = unserialize($pp['parameter']);
//echo "<pre>";
//print_r($pp1);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>    
    jQuery(document).ready(function () {
        jQuery.ajax({url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=30.7046486,76.71787259999996&sensor=true&key=AIzaSyCGgIRJSP3e6zpYXOIlIDf6wakrmvwUoEI',
            success: function (data) {
                alert(data.results[0].formatted_address);

            }
        });

    });
</script>
<?php
$geocode_stats = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=30.7046486,76.71787259999996&sensor=false&key=AIzaSyCGgIRJSP3e6zpYXOIlIDf6wakrmvwUoEI");
$output_deals = json_decode($geocode_stats);
$country = $output_deals->results[2]->address_components[4]->long_name;
echo $country;
?>