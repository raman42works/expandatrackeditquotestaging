<?php

class Order extends Model {

    public function showAllServices() {
        $sql = "SELECT * FROM service";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createNewOrder($user_id, $name, $email, $phone, $address, $quote_date, $expires, $quotation, $quoteno, $travel_cost, $is_include, $manual_cost, $is_manual_include, $subscription, $taxstatus, $service_cost, $other, $other_text, $description,$signature) {
        $sql = "INSERT INTO `orders` (`id`, `user_id`, `name`, `email`, `phone`, `address`, `quote_date`, `expires`, `quotation`, `quoteno`, `travel_cost`, `is_include`, `manual_cost`, `is_manual_include`, `subscription`, `taxstatus`, `service_cost`, `other`, `other_text`, `description`, `signature`) VALUES (NULL, '1', '$name', '$email', '$phone', '$address', '$quote_date', '$expires', '$quotation', '$quoteno', '$travel_cost', '$is_include', '$manual_cost', '$is_manual_include', '$subscription', '$taxstatus', '$service_cost', '$other', '$other_text', '$description','$signature')";
        $lastId = '';
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            'title' => $name,
            'description' => $email,
        ]);
        return $lastId = Database::getBdd()->lastInsertId();
    }

//    public function updatetheOrder($orderid,$name, $email, $phone, $address, $qdate, $edate, $extranote, $taxstatus) {
//        $sql = "INSERT INTO `orders` (`id`, `user_id`, `name`, `email`, `phone`, `address`, `quote_date`, `expires`, `quotation`, `quoteno`, `travel_cost`, `is_include`, `manual_cost`, `is_manual_include`, `subscription`, `taxstatus`, `service_cost`, `other`, `other_text`, `description`) VALUES (NULL, '15', '$name', '$email', '$phone', '$address', '$qdate', '$edate', '$extranote', '', '', '', NULL, '1', '', '$taxstatus', '', '', NULL, NULL);";
//        $lastId = '';
//        $req = Database::getBdd()->prepare($sql);
//        $req->execute([
//            'title' => $name,
//            'description' => $email,
//        ]);
//        return $lastId = Database::getBdd()->lastInsertId();
//    }

    public function addserviceOrder($orderID, $service_id, $variation_id, $installationcost_id, $meta, $parameter, $created_at, $updated_at, $travel_cost, $description, $qty, $service_cost, $other, $customer_text) {
        $sql = "INSERT INTO `service_order` (`id`, `order_id`, `service_id`, `variation_id`, `installationcost_id`, `meta`, `parameter`, `created_at`, `updated_at`, `travel_cost`, `description`, `qty`, `service_cost`, `other`, `customer_text`) VALUES (NULL, '$orderID', '$service_id', '$variation_id', '$installationcost_id', '$meta', '$parameter', '$created_at', '$updated_at', '$travel_cost', '$description', '$qty', '$service_cost', '$other', '$customer_text');";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $lastId = Database::getBdd()->lastInsertId();
    }

}

?>