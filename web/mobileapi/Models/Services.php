<?php

class Services extends Model {

    public function showAllServices() {
        $sql = "SELECT * FROM service";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllServiceswithTime($date) {
        $sql = "SELECT * FROM service WHERE updatedAt > '$date'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showServicesVariation($serviceID) {
        $sql = "SELECT * FROM parameter WHERE service_id = '$serviceID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllServicesVariation() {
        $sql = "SELECT * FROM parameter";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllServicesVariationwithTime($date) {
        $sql = "SELECT * FROM parameter WHERE updatedAt > '$date'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showVariationOption($variationID) {
        $sql = "SELECT * FROM parameter WHERE parent_id = '$variationID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showVariationsWithServiceID($serviceID) {
        $sql = "SELECT * FROM variation WHERE service_id = '$serviceID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllVariations() {
        $sql = "SELECT * FROM variation";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllVariationswithTime($date) {
        $sql = "SELECT * FROM variation WHERE updatedAt > '$date'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showVariationPrice($serviceID, $height, $width) {
        $sql = "SELECT * FROM variation WHERE service_id = '$serviceID' AND ($height between start_height and end_height) AND ($width between start_with and end_with)";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showVariationID($serviceID, $height, $width) {
        $sql = "SELECT id FROM variation WHERE service_id = '$serviceID' AND ($height between start_height and end_height) AND ($width between start_with and end_with)";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function instalationsByServiceId($serviceID) {
        $sql = "SELECT * FROM installationcost WHERE service_id = '$serviceID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllinstalationsCost() {
        $sql = "SELECT * FROM installationcost";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showAllinstalationsCostwithTime($date) {
        $sql = "SELECT * FROM installationcost WHERE updatedAt > '$date'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showinstalationID($serviceID, $width) {
        $sql = "SELECT id FROM installationcost WHERE service_id = '$serviceID' AND ($width between start_range and end_range)";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function showinstalationCost($instalationID) {
        $sql = "SELECT cost FROM installationcost WHERE id = '$instalationID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function showparameterCost($parameterID) {
        $sql = "SELECT cost FROM parameter WHERE id = '$parameterID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $mkk = $req->fetchAll(PDO::FETCH_ASSOC);
        return $mkk[0]['cost'];
    }

    public function showInstaltionOfService($serviceID, $width) {
        $sql = "SELECT * FROM installationcost WHERE service_id = '$serviceID'";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll();
    }

    public function addserviceOrder($orderid, $serviceID, $variationID, $instaltionID, $meta, $serlizedVariation, $createdDate, $qty, $customertext) {
        $sql = "INSERT INTO `service_order` (`id`, `order_id`, `service_id`, `variation_id`, `installationcost_id`, `meta`, `parameter`, `created_at`, `updated_at`, `travel_cost`, `description`, `qty`, `service_cost`, `other`, `customer_text`) VALUES (NULL, '$orderid', '$serviceID', '$variationID', '$instaltionID', '$meta', '$serlizedVariation', '$createdDate', '$createdDate', '', NULL, '$qty', '', '', '$customertext');";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $lastId = Database::getBdd()->lastInsertId();
    }

}

?>