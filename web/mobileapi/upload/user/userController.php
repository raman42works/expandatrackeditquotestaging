<?php

class userController extends Controller {

    function index() {
        $mmk = require(ROOT . 'Models/User.php');
        $d[] = '';
        $d['Users'] = '';
        $User = new User();

        if (isset($_POST["username"])) {
            $d['Users'] = $User->checkUser($_POST["username"], $_POST["pass"]);
            header("Location: " . WEBROOT . "user/index");
        }

        $this->set($d);
        $this->render("index");
    }

    /**
     * Get UserInfo
     * @param Username/Useremail
     * @param Password
     * @return Service Variation 
     */
    function validateLogin() {
        $mmk = require(ROOT . 'Models/User.php');
        $d[] = '';
        $userData = '';
        $User = new User();

        $response = array();
        if ($_POST['token'] == 'ExpandABestTrack951') {
            if ($_POST['username'] == '' || $_POST['pass'] == '') {
                $response = array(
                    'status' => "false",
                    'message' => 'Username/Password Required.'
                );
            } else {
                $username = $_POST['username'];
                $password = $_POST['pass'];
                $d['Users'] = $User->checkUser($_POST["username"], $_POST["pass"]);
                $userData = $d['Users'];
                if ($userData) {
                    $response = array(
                        'status' => "true",
                        'flag' => "new_changes",
                        'flag_message' => "Yes its New.",
                        'user_data' => $userData,
                    );
                } else {
                    $response = array(
                        'status' => "false",
                        'message' => "Not a valid username and password."
                    );
                }
            }

            // send response        
        } else {
            $response = array(
                'status' => "false",
                'message' => 'Token Required.'
            );
        }
        echo json_encode($response);
    }

    /**
     * Get UserInfo
     * @param Userid and Token
     * @return UserInfo
     */
    function getUserData() {
        $mmk = require(ROOT . 'Models/User.php');
        $userID = $_POST['userid'];

        // $userID = 5;
        $d[] = '';
        $userData = '';
        $User = new User();
        $response = array();
        $resultdata = array();
        //if ($_POST['token'] == 'ExpandABestTrack951') {
            if ('ExpandABestTrack951' == 'ExpandABestTrack951') {
            if ($userID) {
                $d['Users'] = $User->showUser($userID);
                $userData = $d['Users'];
                $resultdata['id'] = $userData['id'];
                $resultdata['fname'] = $userData['fname'];
                $resultdata['lname'] = $userData['lname'];
                $resultdata['email'] = $userData['email'];
                $resultdata['phone'] = $userData['phone'];
                $resultdata['address'] = $userData['address'];
                $resultdata['bio'] = $userData['bio'];
                $resultdata['path'] = $userData['path'];

                if ($resultdata) {
                    $response = array(
                        'status' => "true",
                        'message' => 'successful',
                        'user_data' => $resultdata,
                    );
                }
                // send response        
            } else {
                $response = array(
                    'status' => "false",
                    'message' => 'Userid Required'
                );
            }
        } else {
            $response = array(
                'status' => "false",
                'message' => 'Token Required'
            );
        }
        echo json_encode($response);
    }
    
    /**
     *  Update User Information
     */

    function updateUserData() {
        $mmk = require(ROOT . 'Models/User.php');
        $token = $_POST['token'];
        $userID = $_POST['userid'];
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $phone = $_POST['phone'];
        $address = $_POST['address'];
        $bio = $_POST['bio'];
        $d[] = '';
        $userData = '';
        $User = new User();
        $response = array();
        $resultdata = '';
        if ($token == 'ExpandABestTrack951') {
            //if ('ExpandABestTrack951' == 'ExpandABestTrack951') {
            if ($userID) {
                if ($_FILES["path"]) {
                    $target_dir = ROOT . "upload/user/";
                    $randnum = rand(156, 1954855);
                    $target_file = $target_dir . $randnum . basename($_FILES["path"]["name"]);
                    if (move_uploaded_file($_FILES["path"]["tmp_name"], $target_file)) {
                        //echo "The file " . $randnum.basename($_FILES["path"]["name"]) . " has been uploaded.";
                        $totalpath = 'http://198.211.119.12/mobileapi/upload/user/' . $randnum . basename($_FILES["path"]["name"]);
                    } else {
                        $totalpath = ' ';
                    }
                }  else {
                    $totalpath = $_POST['path'];
                }
                $d['Users'] = $User->UpdateUser($userID, $fname, $lname, $phone, $address, $bio, $totalpath);
                $resultdata = $d['Users'];
                if ($resultdata == 'Sucussfull') {
                    $response = array(
                        'status' => "true",
                        'message' => 'successful',
                        'data' => $resultdata,
                        'path' => $totalpath
                    );
                } else {
                    $response = array(
                        'status' => "false",
                        'message' => 'There is error in updating user profile',
                        'data' => $resultdata,
                    );
                }
            } else {
                $response = array(
                    'status' => "false",
                    'message' => 'Userid Required'
                );
            }
        } else {
            $response = array(
                'status' => "false",
                'message' => 'Token Required'
            );
        }
        echo json_encode($response);
    }

    
    
}

?>