<?php

class Database
{
    private static $bdd = null;

    private function __construct() {
    }

    public static function getBdd() {
        if(is_null(self::$bdd)) {
            self::$bdd = new PDO("mysql:host=127.0.0.1;dbname=expandatrack", 'root', '2f2f707fd61f4eca');
        }
        return self::$bdd;
    }
}
?>